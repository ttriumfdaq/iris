/********************************************************************\
  Name:         sctemp.cxx
  Created by:   PAA
  $Id$
                Monitor temperature through the LakeShore-meter
                using the GPIB/MSCB interface
\********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "midas.h"
#include "mscb.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

  /*-- Globals -------------------------------------------------------*/

  /* The frontend name (client name) as seen by other MIDAS clients   */
  char const *frontend_name = "IonTemp";
  /* The frontend file name, don't change it */
  char const *frontend_file_name = __FILE__;

  /* frontend_loop is called periodically if this variable is TRUE    */
  BOOL frontend_call_loop = TRUE;

  /* a frontend status page is displayed with this frequency in ms */
  INT display_period = 0000;

  /* maximum event size produced by this frontend */
  INT max_event_size = 10000;

  /* maximum event size for fragmented events (EQ_FRAGMENTED) */
  INT max_event_size_frag = 5 * 1024 * 1024;

  /* buffer size to hold events */
  INT event_buffer_size = 100 * 10000;

  //  char mydevname[] = {"GENERICIO410"};
  //  char mydevname[]= {"GENERIC_DAQ_V2"};
  char mydevname[]= {"TEMP36"};
  HNDLE hDB, hDD, hSet, hControl;
  /*
#define TEMP_SETTINGS_STR(_name) char const *_name[] = {\
    "[DD]",\
    "MSCB Device = STRING : [32] mscb508.triumf.ca",\
    "MSCB Pwd = STRING : [32] ",\
    "Base Address = INT : 1",\
    "",\
    "[.]",\
    "Names = STRING[5] :",\
    "[32] Int Temp",\
    "[32] Ext Temp1",\
    "[32] Ext Temp2",\
    "[32] Rh Temp",\
    "[32] Rh Hum",\
    "",\
    NULL }
  */

#define TEMP_SETTINGS_STR(_name) char const *_name[] = {\
    "[DD]",\
    "MSCB Device = STRING : [32] mscb508.triumf.ca",\
    "MSCB Pwd = STRING : [32] ",\
    "Base Address = INT : 1",\
    "",\
    "[.]",\
    "Names = STRING[2] :",\
    "[32] Int Temp",\
    "[32] Int Temp2",\
    "",\
    NULL }

typedef struct {
    struct {
      char      mscb_device[32];
      char      mscb_pwd[32];
      INT       base_address;
    } dd;
    char      names[3][32];
  } TEMP_SETTINGS;

  TEMP_SETTINGS temp_settings;
  int lmscb_fd;
  
  /*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_trigger_event(char *pevent, INT off);
  INT read_scaler_event(char *pevent, INT off);
  INT read_mscb_event(char *pevent, INT off);
  INT localmscb_init(char const *eqname);
  void param_callback(INT hDB, INT hKey, void *info);

  /*-- Equipment list ------------------------------------------------*/

  EQUIPMENT equipment[] = {
    {"IonTemp",                 /* equipment name */
     {18, 0,                   /* event ID, trigger mask */
      "",                      /* event buffer */
      EQ_PERIODIC,   /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |   /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      10000,                  /* read every 20 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", "",},
     read_mscb_event,       /* readout routine */
    },

    {""}
  };

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int status;
  char bval;

  /* hardware initialization */
  /* print message and return FE_ERR_HW if frontend should not be started */
  printf("status:%d\n");
  status = localmscb_init("IonTemp");
  if (status != FE_SUCCESS) {
    cm_msg(MERROR,"feSctemp","Access to mscb failed [%d]", status);
    return status;
  }
  
  // Enable mode 4 loop
  bval = 4;
  status = mscb_write(lmscb_fd, temp_settings.dd.base_address, 5, &bval, 1);

  printf("localmscb_init status:%d\n", status);
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  /* put here clear scalers etc. */
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(100);
  return SUCCESS;
}

/*------------------------------------------------------------------*/
// Readout routines for different events
/*-- Trigger event routines ----------------------------------------*/

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  int i;
  DWORD lam;
  
  for (i = 0; i < count; i++) {
    lam = 1;
    if (lam & LAM_SOURCE_STATION(source))
      if (!test)
	return lam;
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- MSCB event --------------------------------------------------*/
INT read_mscb_event(char *pevent, INT off)
{
  int status, size;
  float fvalue, itemp, *pfdata;
  char str[64];

  /* init bank structure */
  bk_init(pevent);

  /* create FDG LakeS Ctm Scm bank */
  bk_create(pevent, "IONP", TID_FLOAT, (void **) &pfdata);
  for (int i=0;i<2;i++) {   // 5
    size = sizeof(fvalue);
    status = mscb_read(lmscb_fd, temp_settings.dd.base_address, 84+i, &fvalue, &size); //39
    if (status != MSCB_SUCCESS) {
      cm_msg(MINFO,"sctemp","mscb_read failed [%d] on %d-ch%d"
	     , status, temp_settings.dd.base_address,84+i);  // 39
    }
    if (i==0) {  // 1
       itemp = fvalue;
    }
    if (i==0) {  // 4
      sprintf(str, "IC Temp: %5.1fC", itemp);
      set_equipment_status("IonTemp", str, "#00FF00"); //"#daa520");
    }
    *pfdata++ = fvalue;
  }

  bk_close(pevent, pfdata);
  return bk_size(pevent);
}

/*-- Local MSCB event --------------------------------------------------*/
INT localmscb_init(char const *eqname)
{
  int  status, size;
  MSCB_INFO node_info;
  char set_str[80];
  TEMP_SETTINGS_STR(temp_settings_str);
  
  cm_get_experiment_database(&hDB, NULL);
  
  /* Map /equipment/Trigger/settings for the sequencer */
  sprintf(set_str, "/Equipment/%s/Settings", eqname);
  //  status = db_create_key(hDB, 0, set_str, TID_KEY);
  
  /* create PARAM settings record */
  status = db_create_record(hDB, 0, set_str, strcomb(temp_settings_str));
  if (status != DB_SUCCESS)  return FE_ERR_ODB;
  
  /* create MSCB settings record */
  //  status = db_find_key (hDB, 0, set_str, &hSet);
  //  status = db_create_record(hDB, hSet, "DD", LMSCB_SETTINGS_STR);

  status = db_find_key(hDB, 0, set_str, &hSet);
  status = db_find_key(hDB, hSet, "DD", &hDD);
  if (status == DB_SUCCESS) {
    size = sizeof(temp_settings.dd);
    db_get_record(hDB, hDD, &(temp_settings.dd), &size, 0);

    int debug = FALSE;
    
    /* Open device on MSCB */
    lmscb_fd = mscb_init(temp_settings.dd.mscb_device, sizeof(temp_settings.dd.mscb_device)
			 , temp_settings.dd.mscb_pwd, debug);
    if (lmscb_fd < 0) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot access MSCB submaster at \"%s\". Check power and connection.",
	     temp_settings.dd.mscb_device);
      return FE_ERR_HW;
    }
    
    // check if devices is alive 
#if 1
    status = mscb_ping(lmscb_fd, (unsigned short) temp_settings.dd.base_address, 1);
    if (status != FE_SUCCESS) {
      cm_msg(MERROR, "mscb_init",
	     "Cannot ping MSCB address %d. Check power and connection.", temp_settings.dd.base_address);
      //      return FE_ERR_HW;
    }
#endif
    
    // Check for right device
    status = mscb_info(lmscb_fd, (unsigned short) temp_settings.dd.base_address, &node_info);
    if (strcmp(node_info.node_name, mydevname) != 0) {
      cm_msg(MERROR, "mscb_init",
	     "Found one expected node \"%s\" at address \"%d\"."
	     , node_info.node_name, temp_settings.dd.base_address);
      return FE_ERR_HW;
    }
    printf("device %s found\n", node_info.node_name);
  }
  return 1; // FE_SUCCESS;
}
