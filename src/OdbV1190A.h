/***************************************************************************/
/*                                                                         */
/*  Filename: OdbV1190A.h                                                  */
/*                                                                         */
/*  Function: headerfile for the CAEN V1190A                               */
/*                                                                         */
/* ----------------------------------------------------------------------- */
/*                                                                         */
/***************************************************************************/

#ifndef  ODBV1190A_INCLUDE_H
#define  ODBV1190A_INCLUDE_H

typedef struct {
  INT       setup;
  INT       leresolution;
  INT       windowoffset;
  INT       windowwidth;
} V1190A_CONFIG_SETTINGS;

// Resolution     0x0   > 100ps
// Window Width   0x14  > 500ns
// Window offset  0xFD8 > -1us
#define V1190A_CONFIG_SETTINGS_STR(_name) const char *_name[] = { \
"Setup = INT : 0", \
"LeResolution = INT : 0", \
"WindowOffset = INT : 0xFD8", \
"WindowWidth = INT : 0x14", \
NULL }

#endif  //  ODBV1190A_INCLUDE_H
