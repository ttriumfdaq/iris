/******************************************************************** \

  Name:         feiris.cxx
  Created by:   

  Contents:    C code example of standarized frontend dealing with
  common VME module at Triumf.
  Used with the VMIC Fanuc VME processor board. 

  $Id$
\********************************************************************/
// Defined in the Makefile 
//#undef V1190_CODE

// Defined in the Makefile
//#define MESADC32_CODE

#define HAVE_IO32

// Defined in the Makefile for the V1190_CODE
//#define HAVE_IO32_TDC

// Defined in the Makefile for the MESADC32_CODE
//#define HAVE_IO32_ADC

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "midas.h"

extern "C" {
#include "mvmestd.h"
//#include "vmicvme.h"
#include "gefvme.h"

#ifdef HAVE_IO32
#include "VMENIMIO32.h"
VMENIMIO32* io32 = NULL;
#endif

#if defined V1190_CODE
#include "v1190drv.h"
#include "OdbV1190A.h"
#include "OdbV1190B.h"
#endif

#ifdef MESADC32_CODE
#include "mesadc32drv.h"
#include "OdbMesadc32.h"
#endif
}

// VMEIO definition
#define P_BOE      0x1  // 
#define P_EOE      0x2  // 
#define P_CLR      0x4  // 
#define S_READOUT  0x8  //
#define S_RUNGATE  0x10 //

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
#ifdef MESADC32_CODE
char const *frontend_name = "feAdc";
#endif
#ifdef V1190_CODE
char const *frontend_name = "feTdc";
#endif
/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 90000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 100000;

/* Hardware */
MVME_INTERFACE *myvme, *gVme;

/* VME base address */
#define N_MESADC32  16
#define N_V1190A   2
#define N_V1190B   4

#ifdef MESADC32_CODE
#define EQ_TRIG_NAME  "AdcTrig"
#define EQ_SCAL_NAME  "AdcScaler" 
#define EV_BUFFER     "BUF01"
#endif
#ifdef V1190_CODE
#define EQ_TRIG_NAME  "TdcTrig"
#define EQ_SCAL_NAME  "TdcScaler"
#define EV_BUFFER     "BUF02"
#endif


// Global 
extern INT run_state;
uint32_t gATscaler;


// VMENIMIO32
int gVmeio32base  = 0x100000;

// Module assignment for each bank of the MESADC32
typedef struct {
  INT nb;
  char bkname[5];
  DWORD base[4];
} MESADC32_STRUCT;
  MESADC32_STRUCT  mes32[]  = {  { 2, "ICA_", { 0x800000  , 0x900000 } }
			       , { 2, "SD2A", { 0xA00000, 0xA10000} }
			       , { 2, "SD1A", { 0xB00000, 0xB10000} }
			       , { 4, "YDA_", { 0xC00000, 0xC10000, 0xC20000, 0xC30000} }
			       , { 2, "SUA_", { 0xD00000, 0xD10000} }
			       , { 4, "YUA_", { 0xE00000, 0xE10000, 0xE20000, 0xE30000} }
		             , { 0, "",     {0}}
                            };

// Base address for configuration
DWORD MESADC32_BASE[N_MESADC32];

//                                       128ch       128ch
DWORD V1190A_BASE[N_V1190A]            = {0xF00000  , 0xF10000};
char  V1190A_BKNAME[N_V1190A][5]       = {"YDT_"    , "YUT_"};

//                                        64ch        64ch       64ch         64ch
DWORD V1190B_BASE[N_V1190B]            = {0xF20000  , 0xF30000,  0xF40000, 0xF50000};
char  V1190B_BKNAME[N_V1190B][5]       = {"SUT_"    , "SD2T",    "SD1T",  "ICT_"};

  int  gTdcBase[]      = { 0xF00000  , 0xF10000, 0xF20000, 0xF30000, 0xF40000, 0xF50000, 0x0};
  int  gTdcAdjust[]    = { 0,          0,        0,        0,        0,        0};
char gTdcBkName[][5] = {"YDT_"     , "YUT_"  , "SUT_"  , "SD2T"  , "SD1T"  , "ICT_"};

/* Globals */
extern HNDLE hDB;
#ifdef MESADC32_CODE
HNDLE hSetMes[N_MESADC32];
MESADC32_CONFIG_SETTINGS tsmes[N_MESADC32];
DWORD ERRate;
HNDLE herrK;
#endif

#ifdef V1190_CODE
HNDLE hSetTdcA[N_V1190A];
V1190A_CONFIG_SETTINGS tsTdcA[N_V1190A];

HNDLE hSetTdcB[N_V1190B];
V1190B_CONFIG_SETTINGS tsTdcB[N_V1190B];
#endif

/* number of channels */
#define N_SCLR 4

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
extern void interrupt_routine(void);
int enable_trigger(void);
int disable_trigger(void);
int clear_busy(void);
int clear_scalers();
INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);
INT read_io32scaler(char *pevent, INT off);
void clear_io32scaler();

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {


   {EQ_TRIG_NAME,            /* equipment name */
#ifdef MESADC32_CODE
    {1, 0,                   /* event ID, trigger mask */
#endif
#ifdef V1190_CODE
    {2, 0,                   /* event ID, trigger mask */
#endif
     //     "SYSTEM",              /* event buffer */
     EV_BUFFER,              /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_POLLED | EQ_EB,      /* equipment type */
#endif
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,             /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    NULL, NULL,
    NULL,
    }
   ,

   {EQ_SCAL_NAME,            /* equipment name */
#ifdef MESADC32_CODE
    {3, 0,                   /* event ID, trigger mask */
#endif
#ifdef V1190_CODE
    {4, 0,                   /* event ID, trigger mask */
#endif
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,            /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     1000 ,                  /* read every 1 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      /* log history */
     "", "", "",},
    read_io32scaler,         /* readout routine */
    },

   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

void FillBaseAddress(void) {
  int k = 0, i = 0;

  while (mes32[i].nb) {
    for (int j=0;j<mes32[i].nb;j++) {
      MESADC32_BASE[k] = mes32[i].base[j];
      printf("MESADC32_BASE[0x%x] -> Bank:%s\n", MESADC32_BASE[k], mes32[i].bkname);
      k++;
    }
    i++;
  }
}
	  
/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_Mescallback(INT hDB, INT hseq, void *info)
{
  printf("odb ... Mes trigger settings touched\n");
}

/*-- Sequencer callback info  --------------------------------------*/
void seq_Tdccallback(INT hDB, INT hseq, void *info)
{
  printf("odb ... Tdc trigger settings touched\n");
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{

#ifdef MESADC32_CODE
  {
    int size, status;
    char set_str[80];

    cm_set_transition_sequence(TR_START, 401);
    cm_set_transition_sequence(TR_STOP, 401);

    // Extract the base address of all the modules from the MES32 struct definition
    FillBaseAddress();
    
    MESADC32_CONFIG_SETTINGS_STR(mesadc32_config_settings_str);
    
    /* Map MESADC32 settings */
    for (int board=0; board<N_MESADC32; board++) {
      sprintf(set_str, "Initializing board-%01d", board);
      set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");
      printf("Setting/Checking Mesadc32 board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/MADC32-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(mesadc32_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetMes[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(MESADC32_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetMes[board], &tsmes[board], size, MODE_READ
				   , seq_Mescallback, NULL)) != DB_SUCCESS)
	return status;
      
    } // Loop over boards                                                                                      
  }
#endif
  
#ifdef V1190_CODE
  {
    int size, status;
    char set_str[80];
    
    cm_set_transition_sequence(TR_START, 402);
    cm_set_transition_sequence(TR_STOP, 402);

    V1190A_CONFIG_SETTINGS_STR(v1190a_config_settings_str);
    for (int board=0; board<N_V1190A; board++) {
      printf("Setting/Checking V1190A board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/V1190A-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(v1190a_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetTdcA[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(V1190A_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetTdcA[board], &tsTdcA[board], size, MODE_READ
				   , seq_Tdccallback, NULL)) != DB_SUCCESS)
	return status;
    } // Loop over boards                                                                                      
    V1190B_CONFIG_SETTINGS_STR(v1190b_config_settings_str);
    /* Map TDCs settings */
    for (int board=0; board<N_V1190B; board++) {
      printf("Setting/Checking V1190B board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/V1190B-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(v1190b_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetTdcB[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(V1190B_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetTdcB[board], &tsTdcB[board], size, MODE_READ
				   , seq_Tdccallback, NULL)) != DB_SUCCESS)
	return status;
    } // Loop over boards
  } // V1190_CODE
#endif
  
  // Open VME interface   
  mvme_open(&myvme, 0);
  gVme = myvme;
  
#ifdef HAVE_IO32_ADC
  if (!io32)
    io32 = new VMENIMIO32(myvme, gVmeio32base);
  
  uint32_t rev = io32->read32(IO32_REVISION); // read firmware revision
  cm_msg(MINFO, frontend_name, "ADC trigger: VMENIMIO32 firmware revision 0x%08x", rev);
  io32->status();

  io32->write32(IO32_NIMOUT, 0x0000); // clear NIM output
  io32->NimOutput(1<<1, 0); // set NIM output 1 (busy)
  io32->SetNimOutputFunction(1, 1); // set NIM output 1 function 1 - daq busy
  io32->write32(IO32_NIMIN, 0xFFFF); // clear NIM input latch
  io32->write32(IO32_TRIG_COUNT, 0); // reset trigger counter
#endif

#ifdef HAVE_IO32_TDC
  if (!io32)
    io32 = new VMENIMIO32(myvme, gVmeio32base);
  
  uint32_t rev = io32->read32(IO32_REVISION); // read firmware revision
  cm_msg(MINFO, frontend_name, "TDC trigger: VMENIMIO32 firmware revision 0x%08x", rev);
  io32->status();

  io32->write32(IO32_NIMOUT, 0x0000); // clear NIM output

  io32->write32(IO32_NIMOUT, 0x0000); // clear NIM output
  io32->NimOutput(1<<1, 0); // set NIM output 1 (busy)
  io32->SetNimOutputFunction(1, 1); // set NIM output 1 function 1 - daq busy

  io32->SetNimOutputFunction(2, 3); // set NIM output 2 function 3 - V1190 TDC trigger synched with V1190 40 MHz clock
  
  io32->write32(IO32_NIMIN, 0xFFFF); // clear NIM input latch
  
  io32->write32(IO32_TRIG_COUNT, 0); // reset trigger counter
#endif

#ifdef MESADC32_CODE
  // Setup parameter for EPICS refresh rate
  int size = sizeof(ERRate);
  db_get_value(hDB, 0, "/Misc/Epics Refresh Rate (ms)", &ERRate, &size, TID_DWORD, TRUE);
  db_find_key(hDB, 0, "/Misc/Epics Refresh Rate (ms)", &herrK);
  db_open_record(hDB, herrK, &ERRate, sizeof(ERRate), MODE_READ, NULL, NULL);

  // Set ECL termination for the last MESAD32 slot 4 G0 & FC
  mesadc32_RegisterWrite(myvme, MESADC32_BASE[0], MESADC32_ECL_TERM, 0x3);
#endif
#ifdef V1190_CODE
  set_equipment_status(EQ_TRIG_NAME, "Initializing TDCs...", "#F0F0AF");

  //-------- TDCs -------------------------------------------
  int csr, temp;

  for (int board=0; board<N_V1190A; board++) {
    char set_str[80];
    printf("--------------- Loading V1190A-%01d ---------------\n", board);
    sprintf(set_str, "Loading V1190A-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");

    // Check the TDC
    csr = mvme_read_value(myvme, V1190A_BASE[board]+V1190_FIRM_REV_RO);
    printf("Firmware revision: 0x%x\n", csr);

    if (tsTdcA[board].setup == 0) {
      // Manual mode
      cm_msg(MINFO, "V1190A", "V1190A in Manual mode");
    } else {
      v1190_Setup(myvme, V1190A_BASE[board], tsTdcA[board].setup);
      
      printf("--------------- Update V1190A-%01d ---------------\n", board);
      // Update some of the main parameters
      if (tsTdcA[board].leresolution == 100)
	temp = LE_RESOLUTION_100;  // 0x3
      else if (tsTdcA[board].leresolution == 200)
	temp = LE_RESOLUTION_200;  // 0x1
      else if (tsTdcA[board].leresolution == 800)
	temp = LE_RESOLUTION_800;  // 0x0
      else
	temp = 3; // 0x3
      //      if (temp != 2) v1190_LEResolutionSet(myvme, V1190A_BASE[board], (WORD) temp); // set 100ps by default
      printf("-------------------------------------------------------------resolution: 0x%x \n", temp);
      v1190_LEResolutionSet(myvme, V1190B_BASE[board], temp);
      
      // Adjust window width given in ns
      // 25ns/bin
      temp = (DWORD) abs(tsTdcA[board].windowwidth / 25);
      printf("windowWidth :0x%x\n", (WORD) temp);
      v1190_WidthSet(myvme, V1190A_BASE[board], (WORD) temp);
      
      // Adjust offset width given in ns
      // 10us == 0xE6D, 1us = 0xF5D
      temp = (tsTdcA[board].windowoffset / 25) + 2;
      temp = temp < 0 ? ((DWORD)temp&0xFFF): (DWORD) temp;
      printf("windowOffset :0x%x\n", (WORD) temp);
      v1190_OffsetSet(myvme, V1190A_BASE[board], (WORD) temp);
      
      // Print Current status 
      printf("----------- Status V1190A-%01d ---------------\n", board);
      v1190_Status(myvme, V1190A_BASE[board]);
    }
  }

  for (int board=0; board<N_V1190B; board++) {
    char set_str[80];
    printf("--------------- Loading V1190B-%01d ---------------\n", board);
    sprintf(set_str, "Loading V1190B-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");
    // Check the TDC
    csr = mvme_read_value(myvme, V1190B_BASE[board]+V1190_FIRM_REV_RO);
    printf("Firmware revision: 0x%x\n", csr);
    
    if (tsTdcB[board].setup == 0) {
      // Manual mode
      cm_msg(MINFO, "V1190B", "V1190B in Manual mode");
    } else {
      v1190_Setup(myvme, V1190B_BASE[board], tsTdcB[board].setup);

      printf("--------------- Updating V1190B-%01d ---------------\n", board);
      // Update some of the main parameters
      if (tsTdcB[board].leresolution == 100)
	temp = LE_RESOLUTION_100;
      else if (tsTdcB[board].leresolution == 200)
	temp = LE_RESOLUTION_200;
      else if (tsTdcB[board].leresolution == 800)
	temp = LE_RESOLUTION_800;
      else
	temp = 3;
      //      if (temp != 2) v1190_LEResolutionSet(myvme, V1190B_BASE[board], temp);
      printf("-------------------------------------------------------------resolution: 0x%x \n", temp);
      v1190_LEResolutionSet(myvme, V1190B_BASE[board], temp);
      
      // Adjust window width given in ns
      // 25ns/bin
      temp = (DWORD) abs(tsTdcB[board].windowwidth / 25);
      printf("windowWidth :0x%x\n", temp);
      v1190_WidthSet(myvme, V1190B_BASE[board], temp);
      
      // Adjust offset width given in ns
      // 10us == 0xE6D, 1us = 0xF5D
      temp = (tsTdcB[board].windowoffset / 25) + 2;
      temp = temp < 0 ? ((DWORD)temp&0xFFF): (DWORD) temp;
      printf("windowOffset :0x%x\n", temp);
      v1190_OffsetSet(myvme, V1190B_BASE[board], temp);

      // Print Current status 
      printf("----------- Status V1190B-%01d ---------------\n", board);
      v1190_Status(myvme, V1190B_BASE[board]);
    }
  }
#endif
  set_equipment_status(EQ_TRIG_NAME, "Idle", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Idle", "#F0F0AF");

  printf("frontend_init: done!\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  enable_trigger();
  clear_busy();
  return SUCCESS;
}

static int gHaveRun = 0;
static int gEventNo = 0;
static int gErrorCount = 0;

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{

  gEventNo = 0;
  gErrorCount = 0;

#ifdef HAVE_IO32_ADC
  int size;

  // program the pulse generator

  int pulser_enabled = FALSE;
  size = sizeof(pulser_enabled);
  db_get_value(hDB, 0, "/Equipment/" EQ_TRIG_NAME "/Settings/IO32/Pulser_enabled", &pulser_enabled, &size, TID_BOOL, TRUE);

  double pulser_freq = 10.0;
  size = sizeof(pulser_freq);
  db_get_value(hDB, 0, "/Equipment/" EQ_TRIG_NAME "/Settings/IO32/Pulser_frequency", &pulser_freq, &size, TID_DOUBLE, TRUE);

  uint32_t pulser_clock = 10;

  uint32_t pulser_period = (1.0e9/pulser_freq)/pulser_clock;
  
  cm_msg(MINFO, frontend_name, "trigger: Pulser period %d: %d ns, %f Hz, enabled %d", pulser_period, pulser_period*pulser_clock, 1.0e9/(pulser_period*pulser_clock), pulser_enabled);

  if (pulser_enabled) {
    io32->SetNimOutputFunction(2, 2); // set NIM output 2 function 2 - pulser
    io32->write32(4*49, pulser_period);
  } else {
    io32->SetNimOutputFunction(2, 0); // set NIM output 2 function 0 - what is it? not a pulser, I hope.
  }
#endif

#ifdef V1190_CODE
  for (int i=0; gTdcBase[i]!=0; i++)
    {
      uint32_t b = gTdcBase[i];

      if (!1) // FIXME: actually read some TDC register!
	{
	  cm_msg(MERROR, frontend_name, "init_vme_modules: V1190B TDC at VME address A24 0x%06x is missing!\n", b);
	  return FE_ERR_HW;
	}

      gTdcAdjust[i] = 0;

      int cr = v1190_Read16(gVme, b, V1190_CR_RW);
      cr |= (1<<3); // enable empty event
      cr |= (1<<4); // enable align64
      cr |= (1<<8); // enable event fifo
      v1190_Write16(gVme, b, V1190_CR_RW, cr);
    }
#endif
    
#ifdef MESADC32_CODE
  //-------- MESADC32 ---------------------------------------
  int status, i;
  uint32_t data;

  /* read Mes records settings */
  for (int board=0;board<N_MESADC32;board++) {
    char set_str[80];
    sprintf(set_str, "Loading board-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");

    // Stop acquisition (0x603A W=0)
     mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, MESADC32_ACQ_STOP);
     // Reset Fifo (0x603C W=0)
     mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_FIFO_RESET, 0);

    // Get record from ODB
    size = sizeof(MESADC32_CONFIG_SETTINGS);
    if ((status = db_get_record (hDB, hSetMes[board], &tsmes[board], &size, 0)) != DB_SUCCESS)
      { return status; } 

    // Default setup 
    if (tsmes[board].setup == 0) {
      // Setup basic first
      mesadc32_Setup(myvme, MESADC32_BASE[board], tsmes[board].setup);

      // ECL gate1 input is the timestamp clock
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], 0x6064, 1);
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], 0x6096, 1);

      // Assign the logical address to the module (mod 32) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MODULE_ID, tsmes[board].modid);
      printf("Mod ID     :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_MODULE_ID));
      
      // Set input range (0x6060 0:4V, 1:10V, 2:8V) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_INPUT_RANGE, tsmes[board].input_range);
      printf("Input Range:0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_INPUT_RANGE));
      
      // Set NIM Busy monitoring (0x606E), 0:busy, 1:g0, 2:g1, 3:Cbus, 4:Bfull, 8:ovBuffer->0x6018 not implemented) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_NIM_BUSY, tsmes[board].nimbusy);
      printf("NIM Busy   :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_NIM_BUSY));
      
      // Set GG usage (0x6058), 0:disable, 1:g0, 2:g1->0x6040!=0 not implemented) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_USE_GG, tsmes[board].use_gg);
      printf("GG usage   :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_USE_GG));
      
      // Set Delay[0..1] (0x605[0..2] on 8bit 0:25ns, 1:150ns, then x50ns) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_0, tsmes[board].delay[0]);
      printf("Delay_0    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_0));
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_1, tsmes[board].delay[1]);
      printf("Delay_1    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_1));
      
      // Set Width[0..1] (0x605[4..6] on 8bit x50ns) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_0, tsmes[board].width[0]);
      printf("Width_0    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_0));
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_1, tsmes[board].width[1]);
      printf("Width_1    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_1));
      
      // Set adc resolution (0x6042 W=0[2K])
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_ADC_RESOLUTION, tsmes[board].resolution);
      printf("Resolution :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_ADC_RESOLUTION));
      
      // Set TimeStamp clock Frq (0x6098 16bit internal clock(VME) 16MHz -> 16 => 1us time measurment)
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board],MESADC32_TS_DIVIDER,  tsmes[board].ts_divider);
      printf("TS Divider :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_TS_DIVIDER));
      
      // Set Clock Source (0x6096 2bit VME(internal):0, ext:1)
      //mesadc32_RegisterWrite(myvme, MESADC32_BASE[board],MESADC32_TS_SOURCE,  tsmes[board].ts_source);
      //printf("TS Source  :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_TS_SOURCE));
      
      // CTRA  Reset all counters (0x6090 W 1)
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_RESET_CTL_AB, 1);
      
      // Init (reset) Fifo (0x603C W 1)
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_FIFO_RESET, 1);
      
      // Enable EoE marker for time stamp (0x6038 W=1)
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MARKING_TYPE, MESADC32_MARK_TIME);
      
      // MESADC32 threshold
      for (i=0;i<32;i++) {
	mesadc32_ThresholdSet(myvme, MESADC32_BASE[board], i,  tsmes[board].threshold[i]);
      } 
    }else {
      // setup pre-defined in the mesytec driver
      mesadc32_Setup(myvme, MESADC32_BASE[board], tsmes[board].setup);
      cm_msg(MINFO, "madc32", "Using driver setup mode %d", tsmes[board].setup);
    }

    mesadc32_Status(myvme, MESADC32_BASE[board]);
    
    //------------------------------------------------------------------------------------------
    // MSCF CONTROL
    if (tsmes[board].mscfRC == 1) {
      if (tsmes[board].mscf_presence) {
	for (int mscf=0;mscf<tsmes[board].mscf_presence;mscf++) {
	  // turn on the RC mode
	  mesadc32_MSCF16_RConoff(myvme, MESADC32_BASE[board], mscf, 1);
	  // MSCF Single channel mode
	  //	status = mesadc32_MSCF16_Set(myvme, MESADC32_BASE[board], mscf, 47, 1);
	  //
	  // Set/Get MSCF Threshold
	  for (int reg=5, i=0 ; reg<21 ; reg++, i++) {
	    // mscf: rc_modnum or device address (0..15)
	    //    i: memory address or register Threshold 5..21
	    status = mesadc32_MSCF16_Set(myvme, MESADC32_BASE[board], mscf, reg
					 , tsmes[board].mscf16Threshold[(mscf*16)+i]);
	    mesadc32_MSCF16_Get(myvme, MESADC32_BASE[board], mscf, reg, &data);
	    printf("Thres: mscf(0..15):%d Register:%i idx:%d Value:%d RbckData:%d/0x%x Wstatus:0x%x\n"
		   , mscf, reg, (mscf*16)+i, tsmes[board].mscf16Threshold[(mscf*16)+i], data, data, status);
	  }
	  //
	  // Set/Get MSCF Gain
	  for (int reg=0, i=0 ; reg<4 ; reg++, i++) {
	    // mscf: rc_modnum or device address (0..15)
	    //    i: memory address or register Gain 0..4
	    status = mesadc32_MSCF16_Set(myvme, MESADC32_BASE[board], mscf, reg
					 , tsmes[board].mscf16Gain[(mscf*4)+i]);
	    mesadc32_MSCF16_Get(myvme, MESADC32_BASE[board], mscf, reg, &data);
	    printf("Gain: mscf(0..3):%d Register:%i idx:%d Value:%d RbckData:%d/0x%x Wstatus:0x%x\n"
		   , mscf, reg, (mscf*4)+i, tsmes[board].mscf16Gain[(mscf*4)+i], data, data, status);
	  }
	}
      } 
    } else {
      cm_msg(MINFO, "mscf", "Skipping MSCF[%d] settings", board);
    }
    //    printf("source:0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_ADD_SOURCE));
    //    printf("Reg   :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_ADD_REG));
    //    printf("ModID :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_MODULE_ID));
    //    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_READOUT_RESET, 0);
  }
#endif

  // Clear scalers

  io32->write32(IO32_COMMAND, IO32_CMD_RESET_TS);
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_SCALERS);

  clear_io32scaler();
  io32->write32(IO32_TRIG_COUNT, 0); // reset trigger counter

  // Cosmetics on Web
  set_equipment_status(EQ_TRIG_NAME, "Running", "#00FF00");
  //set_equipment_status(EQ_SCAL_NAME, "Running", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Running", "#00FF00");

  // Clear Scalers
  clear_scalers();

  // Arm modules and Trigger
  clear_busy();
  enable_trigger();

  // Pulse Adc & TDC Reset bus
  //io32->NimOutput(1, 0); // set nimout[0]
  //io32->NimOutput(0, 1); // clear nimout[0]

  gHaveRun = 1;

  cm_msg(MINFO, frontend_name, "begin run %d", run_number);
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  static bool gInsideEndRun = false;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  gHaveRun = 0;
  cm_msg(MINFO, frontend_name, "end of run %d, %d events, %d errors",run_number, gEventNo, gErrorCount);

  disable_trigger();

  gInsideEndRun = false;

#if 0
  for (int board=0;board<N_MESADC32;board++) {
    if (tsmes[board].mscf_presence) {
      for (int mscf=0;mscf<tsmes[board].mscf_presence;mscf++) {
      }
    }
  }
#endif

#if V1190_CODE
  for (int m=0;m<N_V1190A;m++) {
    v1190_SoftClear(myvme, V1190A_BASE[m]);
  }

  for (int m=0;m<N_V1190B;m++) {
    v1190_SoftClear(myvme, V1190B_BASE[m]);
  }
#endif

  set_equipment_status(EQ_TRIG_NAME, "Idle", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Idle", "#F0F0AF");

  //char str[32];
  //sprintf(str, "Taken %d", gATscaler);
  //set_equipment_status(EQ_SCAL_NAME, str, "#F0F0AF");

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  set_equipment_status(EQ_TRIG_NAME, "Paused", "#A0A0FF");
  gHaveRun = 0;
  disable_trigger();
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  set_equipment_status(EQ_TRIG_NAME, "Running", "#00FF00");
  gHaveRun = 1;
  enable_trigger();
  clear_busy();
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
#ifdef MESADC32_CODE
// EPICS
//-PAA added #19 SuOR, #23 Sd1OR
#define N_EPICS 6  // No more than 19/20 (last is the watchdog)
static HNDLE hEpicsVar = 0;
static uint32_t lasttime=0;
static float epicsVar[N_EPICS];
#endif

INT frontend_loop() {

#ifdef MESADC32_CODE
  // EPICS
  if (hEpicsVar == 0) {
    // First time in get key
    int status = db_find_key (hDB, 0, "Equipment/Beamline/Variables/Demand", &hEpicsVar);
    if (status != DB_SUCCESS) {
      cm_msg(MINFO,"FE","EPICS demand not found");
      hEpicsVar = 0;
    } 
  } else {
    if (ss_millitime() - lasttime > ERRate) { // Every 100ms using _index as the 19 is controlled by feEpics
      // Periodically copy local scaler to Epics equipment (BeamLine)
      db_set_data_index(hDB, hEpicsVar, &epicsVar[6], sizeof(float),  0, TID_FLOAT); // SID2
      db_set_data_index(hDB, hEpicsVar, &epicsVar[4], sizeof(float),  1, TID_FLOAT); // SuOR
      db_set_data_index(hDB, hEpicsVar, &epicsVar[5], sizeof(float),  2, TID_FLOAT); // Sd1OR
      db_set_data_index(hDB, hEpicsVar, &epicsVar[3], sizeof(float),  8, TID_FLOAT); // SSB
      db_set_data_index(hDB, hEpicsVar, &epicsVar[0], sizeof(float), 10, TID_FLOAT); // Scint
      db_set_data_index(hDB, hEpicsVar, &epicsVar[1], sizeof(float), 11, TID_FLOAT); // Ion chamber
      db_set_data_index(hDB, hEpicsVar, &epicsVar[2], sizeof(float), 12, TID_FLOAT); // FreeTrg
      //      printf("loop: update: %f %f\n", epicsVar[0], epicsVar[1]);
      lasttime = ss_time();
    }
  }  
#endif

  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

char bars[] = "|\\-/";
int i_bar;

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
     /* Polling routine for events. Returns TRUE if event
	is available. If test equals TRUE, don't return. The test
	flag is used to time the polling */
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE))
    //    if (lam > 10)
{
#if 0 // def MESADC32_CODE
  for (int i = 0; i < count; i++) {
    int lam = mesadc32_RegisterRead(myvme, MESADC32_BASE[0], MESADC32_BUF_DATA_LEN);
    if (lam)
      if (!test)
	return lam;
  }
#endif

  // ADC IO Module
#ifdef HAVE_IO32_ADC
  for (int i = 0; i < count; i++) {
    int lam = io32->read32(IO32_NIMIN);
#if 0
    if (!test) {
      printf("...%c #:%d - l:%d lam:%x\r", bars[i_bar++ % 4], count, i, lam);
      fflush(stdout); 
    }
#endif
    lam &= 0x20000;
    if (lam)
      if (!test) {
	return lam;
      }
  }
#endif

  // TDC IO Module
#ifdef HAVE_IO32_TDC
  for (int i = 0; i < count; i++) {
    int lam = io32->read32(IO32_NIMIN);
#if 0
    if (!test) {
      printf("...%c #:%d - l:%d lam:%x\r", bars[i_bar++ % 4], count, i, lam);
      fflush(stdout); 
    }
#endif
    lam &= 0x20000;
    if (lam)
      if (!test) {
	return lam;
      }
  }
#endif
  
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

//
//---------------------------------------------------------------
int enable_trigger()
{

#ifdef V1190_CODE
  for (int i=0; gTdcBase[i]; i++) {
    v1190_SoftClear(gVme, gTdcBase[i]);
  }
#endif

#ifdef MESADC32_CODE
  for (int board=0;board<N_MESADC32;board++) {
    // Start acquisition (0x603A W=1)
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, 1);
  } 
#endif

#ifdef HAVE_V792
  for (int i=0; gAdcBase[i]; i++) {
    v792_DataClear(gVme, gAdcBase[i]);
  }
#endif

#ifdef HAVE_IO32
  io32->NimOutput(0, 1<<1);          // clear busy output level
#endif

  return 0;
}

//
//---------------------------------------------------------------------
int disable_trigger()
{

#ifdef HAVE_IO32
  io32->NimOutput(1<<1, 0); // set busy output level
#endif

#ifdef MESADC32_CODE
  for (int board=0;board<N_MESADC32;board++) {
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, 0);
  }
#endif

  return 0;
}

//
//---------------------------------------------------------------
int clear_busy()
{
  
#ifdef HAVE_IO32
  io32->write32(IO32_NIMIN, 0x0002); // clear NIM input latch busy bit
#endif
  return 0;
}

//
//---------------------------------------------------------------
int clear_scalers()
{
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_SCALERS);
  return 0;
}

//
//---------------------------------------------------------------
#ifdef V1190_CODE
int read_v1190(char*pevent, int evt_num, bool wait)
{
  int have_data = 0;

  for (int t=0; gTdcBase[t] != 0; t++)
    {
      uint32_t b = gTdcBase[t];

      int fifo_status = 0;
      int fifo_stored = 0;

      if (wait) {
	bool timeout = true;
	for (int itry=0; itry<10000; itry++) {
	  fifo_status = v1190_Read16(gVme, b, 0x103E);
	  if (fifo_status == 3) {
	    fifo_stored = 100;
	    timeout = false;
	    break;
	  }
	  fifo_stored = v1190_Read16(gVme, b, 0x103C);
	  if (fifo_stored) {
	    //if (itry > 0)
	    //deb_printf("v1190 wait %d\n", itry);
	    timeout = false;
	    break;
	  }
	}
	if (timeout) {

	  gErrorCount ++;
	  if (gErrorCount < 100) {

	    cm_msg(MERROR, frontend_name, "v1190 unit %d timeout waiting for data: trigger %d (0x%x)", t, evt_num, evt_num);
	    if (1) {
	      fifo_stored = v1190_Read16(gVme, b, 0x103C);
	      fifo_status = v1190_Read16(gVme, b, 0x103E);
	      int cr  = mvme_read_value(gVme, b+0x1000);
	      int sr  = mvme_read_value(gVme, b+0x1002);
	      int evt = v1190_EvtCounter(gVme, b);
	      
	      cm_msg(MERROR, frontend_name, "v1190 unit %d: cr 0x%04x, sr 0x%04x, evt %d, fifo %d, status %d", t, cr, sr, evt, fifo_stored, fifo_status);
	    }
	  }
	}
      }

      //int fifo_stored = v1190_Read16(gVme, b, 0x103C);

      //printf("tdc %d: fifo_stored: %d\n", t, fifo_stored);

      if (fifo_stored == 0xFFFF)
	break;

      static bool doPrint = 0;
      
      int nw = 0;

      if (fifo_stored > 0)
	{
	  if (fifo_stored > 100)
	    fifo_stored = 100;

	  if (doPrint)
	    printf("tdc %d: fifo_stored: %d\n", t, fifo_stored);

	  for (int i=0; i<fifo_stored; i++)
	    {
	      uint32_t w = v1190_Read32(gVme, b, 0x1038);
	      //int event_count = 0xFFFF&(w>>16);
	      int word_count = w&0xFFFF;
	      //printf("fifo[%d] is 0x%08x, event %d, wc %d\n", i, w, event_count, word_count);

	      nw += word_count;
	    }
	}

      if (nw <= 0)
	continue;

      uint32_t *pdata32;

      bk_create(pevent, gTdcBkName[t], TID_DWORD, (void **) &pdata32);

      int count = v1190_DataRead(gVme, b, pdata32, nw);

      int ec = v1190_Read32(gVme, b, 0x101C);

      uint16_t evt16 = evt_num & 0xFFFF;
      uint16_t eh16  = ((*pdata32)>>5)&0xFFFF;
      uint16_t ec16  = (ec-1) & 0xFFFF;

      eh16 += gTdcAdjust[t];
      ec16 += gTdcAdjust[t];
      //printf("TDC header 0x%08x (event %d), event counter register %d, midas event num %d\n", *pdata32, eh, ec, evt_num);

      if (ec16 != evt16) {
	gTdcAdjust[t] += evt16 - ec16;

	gErrorCount ++;
	if (gErrorCount < 100) {
	  cm_msg(MERROR, frontend_name, "v1190 unit %d event counter mismatch: trigger %d (0x%x), TDC event counter %d (0x%x, correct 0x%04x), new correction %d", t, evt_num, evt_num, ec, ec, ec16, gTdcAdjust[t]);
	  
	  if (1) {
	    int fifo_stored = v1190_Read16(gVme, b, 0x103C);
	    int fifo_status = v1190_Read16(gVme, b, 0x103E);
	    int cr  = mvme_read_value(gVme, b+0x1000);
	    int sr  = mvme_read_value(gVme, b+0x1002);
	    int evt = v1190_EvtCounter(gVme, b);
	    
	    cm_msg(MERROR, frontend_name, "v1190 unit %d: cr 0x%04x, sr 0x%04x, evt %d, fifo %d, status %d", t, cr, sr, evt, fifo_stored, fifo_status);
	  }
	}

	//printf("TDC adjust %d\n", gTdcAdjust[t]);
	//exit(123);
      }

      if (eh16 != evt16) {

	gErrorCount ++;
	if (gErrorCount < 100) {
	  cm_msg(MERROR, frontend_name, "v1190 unit %d event number mismatch: trigger %d (0x%x), corrected TDC data header event number %d (0x%x)", t, evt_num, evt_num, eh16, eh16);
	  if (1) {
	    int fifo_stored = v1190_Read16(gVme, b, 0x103C);
	    int fifo_status = v1190_Read16(gVme, b, 0x103E);
	    int cr  = mvme_read_value(gVme, b+0x1000);
	    int sr  = mvme_read_value(gVme, b+0x1002);
	    int evt = v1190_EvtCounter(gVme, b);
	    
	    cm_msg(MERROR, frontend_name, "v1190 unit %d: cr 0x%04x, sr 0x%04x, evt %d, fifo %d, status %d", t, cr, sr, evt, fifo_stored, fifo_status);
	  }
	  //exit(123);
	}
      }

      pdata32 += count;

      bk_close(pevent, pdata32);

      //if( have_data == 0 ){ printf("-----------------------------event %6d-------------------------\n", evt_num); }

      have_data |= 1;

      if (doPrint)
	printf("tdc %d, fifo_stored: %d, nw %d, count %d!\n", t, fifo_stored, nw, count);
    }

  return have_data;
}
#endif

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{
  //  printf("In read_trigger_event\n");
  // > THE LINE BELOW SHOULD BE HERE AND NOWHERE ELSE < !
  bk_init32(pevent);
  
#ifdef HAVE_IO32
  if (0) {
    static uint32_t xtscctl = -1;
    uint32_t tscctl = io32->read32(4*12);
    if (tscctl != xtscctl) {
      printf("TSC status changed 0x%08x -> 0x%08x, ts 0x%08x, nim_in 0x%08x\n"
	     , xtscctl, tscctl, io32->read32(4*6), io32->read32(4*3));
      xtscctl = tscctl;
    }
  }
  
  // Read IO input looking for the LAM
  uint32_t io32_data = io32->read32(IO32_NIMIN);
  
  // Skip readout if no LAM (upper 16bit are Input latched)
  // Input 0x2 latched
  if ((io32_data & 0x00020000) == 0)
    return 0;
  
  // Register info for statistics 
  uint32_t start_time = io32->read32(IO32_TS); // read the TSC
  uint32_t trig_time  = io32->read32(IO32_TRIG_TS); // trigger timestamp
  uint32_t trig_count = io32->read32(IO32_TRIG_COUNT); // trigger counter
  
#endif  

  //printf("IO32 trigger %d ts 0x%08x, event %d, ts 0x%08x!\n", trig_count, trig_time, gEventNo, start_time);
  
  if (1) {
    // Check if HW & SW trigger counter matches
    if (trig_count != (DWORD) gEventNo+1) {
      gErrorCount ++;
      if (gErrorCount < 100)
	cm_msg(MERROR, frontend_name, "VME trigger problem: event counter expected %d, got %d", gEventNo+1, trig_count);
    }
  }
  
  // Compose readout Bank info
  if (0) {
    uint32_t* pdata32;
#if defined V1190_CODE
    bk_create(pevent, "VTST", TID_DWORD, (void **) &pdata32);
#endif
#if defined MESADC32_CODE
    bk_create(pevent, "VTSA", TID_DWORD, (void **) &pdata32);
#endif 
    *pdata32++ = 0xbbbb0010;
    uint32_t tscctl;

    *pdata32++ = io32->read32(IO32_REVISION);   // firmware revision
    *pdata32++ = io32->read32(IO32_TS);         // 20MHz timestamp
    *pdata32++ = io32->read32(IO32_TSC4_CTRL);  // TSC routing
    *pdata32++ = tscctl = io32->read32(4*12);   // TSC control

    int overflow = tscctl & 0x8000;
    int wc = tscctl & 0x7FFF;

    //printf("TSC ctl: 0x%08x, wc %d\n", tscctl, wc);

    for (int i=0; i<wc; i++)
      *pdata32++ = io32->read32(IO32_TSC4_FIFO); // read TSC FIFO

    if (overflow) {
      io32->write32(IO32_TSC4_FIFO, 0);          // clear TSC overflow flag
      *pdata32++ = 0xFFFFFFFF;                   // write a TSC overflow marker
      cm_msg(MERROR, frontend_name, "VME timestamp fifo overflow, tsc status 0x%08x", tscctl);
    }

    bk_close(pevent, pdata32);
  }

  // Readout sequence
  bool have_data = false;

#if defined V1190_CODE

#if 0
  int nentry;
  // TDC 128 channels
  for (int m=0;m<N_V1190A;m++) {
    // create variable length TDC bank
    bk_create(pevent, V1190A_BKNAME[m], TID_DWORD, (void **) &pdata);
    
    // Read Event 
    v1190_EventRead(myvme, V1190A_BASE[m], pdata, (void **) &nentry);
    pdata += nentry;
    
    // clear TDC 
    //  v1190_SoftClear(myvme, V1190A_BASE[m]);
    bk_close(pevent, pdata);
  }

  // TDC 64 channels
  for (int m=0;m<N_V1190B;m++) {
    // create variable length TDC bank
    bk_create(pevent, V1190B_BKNAME[m], TID_DWORD, (void **) &pdata);
    
    // Read Event 
    v1190_EventRead(myvme, V1190B_BASE[m], pdata, (void **) &nentry);
    pdata += nentry;
    
    // clear TDC 
    //  v1190_SoftClear(myvme, V1190B_BASE[m]);
    bk_close(pevent, pdata);
  }
#else
  if (read_v1190(pevent, gEventNo, true)) {
    have_data = true;
    //printf("event %d: V1190!\n", gEventNo);
  }
#endif

#endif

#if defined MESADC32_CODE
  ///-------------------------------
  DWORD  *pdata;
  int nentry;
  int m = 0;
  
  // Over all the groups
  while (mes32[m].nb) {
    // create MESADC32 bank for the group
    bk_create(pevent, mes32[m].bkname, TID_DWORD, (void **) &pdata);
    // Add all the modules to the current bank
    for (int j=0 ; j<mes32[m].nb ; j++) {

      uint16_t data_ready = 0;
      uint16_t buffer_data_length;

      for (int i=0; i<10; i++) {
	data_ready = mesadc32_RegisterRead(myvme, mes32[m].base[j], 0x603E);
	buffer_data_length = mesadc32_RegisterRead(myvme, mes32[m].base[j], 0x6030);
	if (data_ready)
	  break;
	if (buffer_data_length)
	  break;
      }

      uint16_t evctr_lo = mesadc32_RegisterRead(myvme, mes32[m].base[j], 0x6092);
      uint16_t evctr_hi = mesadc32_RegisterRead(myvme, mes32[m].base[j], 0x6094);

      uint32_t evctr = (evctr_hi<<16) | evctr_lo; 

      if (!data_ready && !buffer_data_length) {
	gErrorCount++;
	if (gErrorCount < 100) {
	  cm_msg(MERROR, frontend_name, "ADC group %d (Bank:%s), module %d, event %d: data not ready: data_ready %d, buffer_data_length %d"
		 , m, mes32[m].bkname, j, evctr, data_ready, buffer_data_length);
	  cm_msg(MERROR, frontend_name, "IO32 NIMIN: 0x%08x", io32->read32(IO32_NIMIN));
	}
      }


      //printf("group %d, module %d, evctr 0x%x 0x%x 0x%x %d, global %d %d\n", m, j, evctr_hi, evctr_lo, evctr, evctr, gEventNo, trig_count);

      if (evctr != (gEventNo+1)) {
	gErrorCount++;
	if (gErrorCount < 100) {
	  cm_msg(MERROR, frontend_name, "ADC group %d (Bankk:%s), module %d, event counter mismatch %d should be %d, data ready %d"
		 , m, mes32[m].bkname, j, evctr, gEventNo+1, data_ready);
	}
      }

      // Get buffer length
      //      nentry = mesadc32_RegisterRead(myvme, mes32[m].base[j], MESADC32_BUF_DATA_LEN);
      //      printf("nentry:%d\n",nentry);
      // Read Module (expect 22xDWORD) 
      nentry = 0;
      mesadc32_ReadData(myvme, mes32[m].base[j], pdata, &nentry);
      pdata += nentry;

      // Reset (0x6034)
      mesadc32_RegisterWrite(myvme, mes32[m].base[j], MESADC32_READOUT_RESET, 0);
    }
    
    // Close bank for the current group
    bk_close(pevent, pdata);
    
    // Next group 
    m++;
  }
#endif  // MESADC32
  
#ifdef HAVE_IO32
  //
  // Readout phase done, record end of readout time (100MHz)
  uint32_t end_time = 0;
  end_time = io32->read32(IO32_TS);             // read the TSC
  
  // Inform End_of_busy
  clear_busy();
  
  // Compose Readout Bank statistics
  {
    uint32_t *pdata32;
#if defined V1190_CODE
    bk_create(pevent, "VTRT", TID_DWORD, (void **) &pdata32);
    *pdata32++ = 0xbbbb0010;  // 0 - IRIS TDC header and version
#endif
#if defined MESADC32_CODE
    bk_create(pevent, "VTRA", TID_DWORD, (void **) &pdata32);
    *pdata32++ = 0xaaaa0010;  // 0 - IRIS ADC header and version
#endif 
    *pdata32++ = trig_count-1;// 1 - event number, counting from 0
    *pdata32++ = trig_time;   // 2 - trigger timestamp
    *pdata32++ = start_time;  // 3 - readout start time
    *pdata32++ = end_time;    // 4 - readout end time
    *pdata32++ = start_time-trig_time; // 5 - trigger latency (trigger time - start of readout time)
    *pdata32++ = end_time-start_time;  // 6 - readout elapsed time
    *pdata32++ = end_time-trig_time;   // 7 - busy elapsed time
    bk_close(pevent, pdata32);
    have_data = true;
  }
#endif // HAVE_IO32

  if (gErrorCount > 0) {
    static int xgErrorCount = 0;

    if (gErrorCount != xgErrorCount) {
      static time_t xgTime = 0;
      time_t now = time(NULL);
      if (xgTime == 0)
	xgTime = now;

      if (now > xgTime + 2)
	{
	  char str[256];
	  sprintf(str, "%d errors", gErrorCount);
	  set_equipment_status(EQ_TRIG_NAME, str, "#FFA0A0");
	  xgErrorCount = gErrorCount;
	  xgTime = now;
	}
    }
  }
  
  // Soft Test lam generator
  //  mesadc32_RegisterWrite(myvme, MESADC32_BASE[0], MESADC32_RESET_CTL_AB, 1);
  //  mesadc32_RegisterWrite(myvme, MESADC32_BASE[0], MESADC32_TESTPULSER, 0x6);
  
  if (have_data) {
    gEventNo ++;
    return bk_size(pevent);
  } else {
    return 0;
  }
}

/*-- Scaler event --------------------------------------------------*/
INT read_scaler_event(char *pevent, INT off)
{
  DWORD *pdata;
  char str[32];
  
  /* init bank structure */
  bk_init(pevent);
  
  gATscaler = SERIAL_NUMBER(pevent);
  if (run_state == RO_RUNNING) {
    sprintf(str, "Running %d", gATscaler);
    set_equipment_status(EQ_SCAL_NAME, str, "#A0A0FF");
  } else {
    sprintf(str, "Taken %d", gATscaler);
    set_equipment_status(EQ_SCAL_NAME, str, "#F0F0AF");
  }

  bool have_data = false;
#ifdef MESADC32_CODE
  /* create SCLA bank */
  bk_create(pevent, "SCLA", TID_DWORD, (void **) &pdata);
  // Latch scalers
  io32->write32(IO32_COMMAND, IO32_CMD_LATCH_SCALERS);
  have_data = true;
  // Read 16 channels
  *pdata++ = io32->read32(IO32_SCALERS_TS);
 for (int i=16;i<32;i++) {
    *pdata++ = io32->read32(IO32_SCALER_0+(i*4)); 
  }
#endif
#ifdef V1190_CODE
  set_equipment_status(EQ_SCAL_NAME, "Running", "#A0A0FF");
  /* create SCLT bank */
  bk_create(pevent, "SCLT", TID_DWORD, (void **) &pdata);
#endif
  
  /* read scaler bank */
  bk_close(pevent, pdata);
  
  if (have_data) {
    return bk_size(pevent);
  } else {
    return 0;
  }
}


const int kMaxIo32scalers = 32;
uint32_t io32scalerSum[kMaxIo32scalers];

//--------------------------------------------------------------------
void clear_io32scaler()
{
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_SCALERS);
  memset(io32scalerSum, 0, sizeof(io32scalerSum));
}

//--------------------------------------------------------------------
INT read_io32scaler(char *pevent, INT off)
{
  //printf("read io32scaler!\n");

  if (0) {
    char str[256];
    gATscaler = SERIAL_NUMBER(pevent);
    if (run_state == RO_RUNNING) {
      sprintf(str, "Running %d", gATscaler);
      set_equipment_status(EQ_SCAL_NAME, str, "#A0A0FF");
    } else {
      sprintf(str, "Taken %d", gATscaler);
      set_equipment_status(EQ_SCAL_NAME, str, "#F0F0AF");
    }
  }

  int numsc  = 32;
  int iclock = 15;

  io32->write32(IO32_COMMAND, IO32_CMD_LATCH_SCALERS);

  /* init bank structure */
  bk_init32(pevent);

  uint32_t psc[numsc];

  // Scaler Data
  {
    uint32_t *pdata32;

#ifdef V1190_CODE
    bk_create(pevent, "SCTD", TID_DWORD, (void **) &pdata32);
#else
    bk_create(pevent, "SCAD", TID_DWORD, (void **) &pdata32);
#endif

    //psc = pdata32;

    int count = io32->ReadScaler(pdata32, numsc);
    assert(count == numsc);

    //Rerouting of scalers after FW 0x01121128
    memcpy(psc, pdata32+16, 16*sizeof(uint32_t));
    memcpy(psc+16, pdata32, 16*sizeof(uint32_t));
    memcpy(pdata32, psc, 32*sizeof(uint32_t));

    pdata32 += numsc;
    
    bk_close(pevent, pdata32);
  }

#ifdef V1190_CODE  
  double dt = 50.0*psc[iclock]/1e9; // 50 ns clock (20MHz)
#else
  double dt = 200.0*psc[iclock]/1e9; // 200 ns clock (5MHz), remember to change
#endif
  double dt1 = 1.0/dt;
  //printf("dt = %f\n", dt);

  // Scaler Rate
  {
    double* pdata;
#ifdef V1190_CODE
    bk_create(pevent, "SCTR", TID_DOUBLE, (void **) &pdata);
#else
    bk_create(pevent, "SCAR", TID_DOUBLE, (void **) &pdata);
#endif
    for (int i=0; i<numsc; i++) {
      *pdata++ = dt1*psc[i];
#if defined MESADC32_CODE
      //Read ADC scaler (PPG32), copy to epics temp array)
      if (i == 29) epicsVar[0] = dt1*psc[i]; // Rate Scintillator
      if (i == 30) epicsVar[1] = dt1*psc[i]; // Rate Ion Chamber
      if (i == 31) epicsVar[2] = dt1*psc[i]; // Rate Free Trigger
      if (i == 27) epicsVar[3] = dt1*psc[i]; // Rate SSB
      if (i == 19) epicsVar[4] = dt1*psc[i]; // SuOR
      if (i == 23) epicsVar[5] = dt1*psc[i]; // Sd1OR
      if (i == 16) epicsVar[6] = dt1*psc[i]; // SID2
#endif
    }
    bk_close(pevent, pdata);
  }

  // Scaler SUM
  {
    DWORD* pdata;
#ifdef V1190_CODE
    bk_create(pevent, "SCTS", TID_DWORD, (void **) &pdata);
#else
    bk_create(pevent, "SCAS", TID_DWORD, (void **) &pdata);
#endif
    for (int i=0; i<numsc; i++) {
      io32scalerSum[i] += psc[i];
      *pdata++ = io32scalerSum[i];
    }
    bk_close(pevent, pdata);
  }

  return bk_size(pevent);
}

// end file
