/******************************************************************** \

  Name:         feiris.cxx
  Created by:

  Note: This is the code for the new, asynchronous readout scheme, developed
  during 2020. The frontend code implementing the old, synchronous (or event-by-
  event), scheme is found in the file feiris_ebye.cxx. /Jonas Refsgaard

  Contents:    C code example of standarized frontend dealing with
  common VME module at Triumf.
  Used with the VMIC Fanuc VME processor board. 

  $Id$
\********************************************************************/
// Defined in the Makefile 
//#undef V1190_CODE

// Defined in the Makefile
//#define MESADC32_CODE

#define HAVE_IO32

// Defined in the Makefile for the V1190_CODE
//#define HAVE_IO32_TDC

// Defined in the Makefile for the MESADC32_CODE
//#define HAVE_IO32_ADC

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h> //usleep

#include "midas.h"

#ifndef EBYE
//Include the stuff relevant for asynchronous readout and memory buffering.
//This is C++, so must go before the 'extern "C"' statement below.
#include "MultiBuffer.h"
#include "EventValidator.h"
#include "SafeQueue.h"
#include <atomic>
#include <mutex> //std::mutex and std::lock_guard (for thread-safety).
#include <pthread.h>
#endif

extern "C" {
#include "mvmestd.h"
//#include "vmicvme.h"
#include "gefvme.h"

#ifdef HAVE_IO32
#include "VMENIMIO32.h"
VMENIMIO32* io32 = NULL;
#endif

#if defined V1190_CODE
#include "v1190drv.h"
#include "OdbV1190A.h"
#include "OdbV1190B.h"
#endif

#ifdef MESADC32_CODE
#include "mesadc32drv.h"
#include "OdbMesadc32.h"
#endif
}

// VMEIO definition
#define P_BOE      0x1  // 
#define P_EOE      0x2  // 
#define P_CLR      0x4  // 
#define S_READOUT  0x8  //
#define S_RUNGATE  0x10 //

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
#ifdef MESADC32_CODE
char const *frontend_name = "feAdc";
#endif
#ifdef V1190_CODE
char const *frontend_name = "feTdc";
#endif
/* The frontend file name, don't change it */
char const *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 90000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 100000;

/* Hardware */
MVME_INTERFACE *myvme, *gVme;

/* VME base address */
#define N_MESADC32  16
#define N_V1190A   2
#define N_V1190B   4

#ifdef MESADC32_CODE
#define EQ_TRIG_NAME  "AdcTrig"
#define EQ_SCAL_NAME  "AdcScaler" 
#define EV_BUFFER     "BUF01"
#endif
#ifdef V1190_CODE
#define EQ_TRIG_NAME  "TdcTrig"
#define EQ_SCAL_NAME  "TdcScaler"
#define EV_BUFFER     "BUF02"
#endif


// Global 
extern INT run_state;
uint32_t gATscaler;


// VMENIMIO32
int gVmeio32base  = 0x100000;

// Module assignment for each bank of the MESADC32
typedef struct {
  INT nb;
  char bkname[5];
  DWORD base[4];
} MESADC32_STRUCT;
  MESADC32_STRUCT  mes32[]  = {  { 2, "ICA_", { 0x800000  , 0x900000 } }
			       , { 2, "SD2A", { 0xA00000, 0xA10000} }
			       , { 2, "SD1A", { 0xB00000, 0xB10000} }
			       , { 4, "YDA_", { 0xC00000, 0xC10000, 0xC20000, 0xC30000} }
			       , { 2, "SUA_", { 0xD00000, 0xD10000} }
			       , { 4, "YUA_", { 0xE00000, 0xE10000, 0xE20000, 0xE30000} }
		             , { 0, "",     {0}}
                            };
//Some convenient variables.
int nAdcBanks = 6;

// Base address for configuration
DWORD MESADC32_BASE[N_MESADC32];

#define IO32_SCALER_ROUTING (4*17)

#ifdef MESADC32_CODE
//MADC32 multicast address.
DWORD MESADC32_MULTICAST_ADDRESS = 0xBB000000;
//Should be in a header file somewhere. 
#define MESADC32_IRQ_THRESHOLD 0x6018
#define MESADC32_MAX_TRANSFER_DATA 0x601A
#endif

#ifdef V1190_CODE
#define V1190_NO_TRIGGER_TIME_SUBTRACT 0x1500
#define V1190_DLL_CLOCK_SOURCE 0xC800
#define V1190_ALMOST_FULL_LEVEL 0x1022
#define V1190_OUT_PROG 0x102C
#endif
std::mutex vme_mutex;    //Mutex used to protect the VME bus from multi-threading.
MultiBuffer *dataBuffer; //Memory buffer to hold the data between readout and validation.
unsigned int maxBufferSize = 1e6; //Block trigger if words in dataBuffer exceeds.
unsigned int minBufferSize = 5e5; //Unblock trigger if buffers size falls below.
//volatile std::atomic<bool> blocked(false);
EventValidator *validator;  //Checks the data in each event.
SafeQueue<uint32_t> tscBuffer; //Buffer for TSC4 time stamp.
pthread_t readout_thread;   //Thread for reading out modules and pushing data to memory.
//pthread_t validate_thread;  //Thread for emptying and validating data in the memory buffer.
volatile std::atomic<bool> stop(true); //Flag to signal to the threads to finish and return.
volatile std::atomic<bool> abort_run(false); //Flag to signal to frontend_loop to do TR_STOP transition.
//volatile std::atomic<bool> readout_active(false); //Flag to signal if the readout thread is active.

//                                       128ch       128ch
DWORD V1190A_BASE[N_V1190A]            = {0xF00000  , 0xF10000};
char  V1190A_BKNAME[N_V1190A][5]       = {"YDT_"    , "YUT_"};

//                                        64ch        64ch       64ch         64ch
DWORD V1190B_BASE[N_V1190B]            = {0xF20000  , 0xF30000,  0xF40000, 0xF50000};
char  V1190B_BKNAME[N_V1190B][5]       = {"SUT_"    , "SD2T",    "SD1T",  "ICT_"};

  int  gTdcBase[]      = { 0xF00000  , 0xF10000, 0xF20000, 0xF30000, 0xF40000, 0xF50000, 0x0};
  int  gTdcAdjust[]    = { 0,          0,        0,        0,        0,        0};
char gTdcBkName[][5] = {"YDT_"     , "YUT_"  , "SUT_"  , "SD2T"  , "SD1T"  , "ICT_"};

/* Globals */
extern HNDLE hDB;
#ifdef MESADC32_CODE
HNDLE hSetMes[N_MESADC32];
MESADC32_CONFIG_SETTINGS tsmes[N_MESADC32];
DWORD ERRate;
HNDLE herrK;
#endif

#ifdef V1190_CODE
HNDLE hSetTdcA[N_V1190A];
V1190A_CONFIG_SETTINGS tsTdcA[N_V1190A];

HNDLE hSetTdcB[N_V1190B];
V1190B_CONFIG_SETTINGS tsTdcB[N_V1190B];
#endif

/* number of channels */
#define N_SCLR 4

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
extern void interrupt_routine(void);
INT read_trigger_event(char *pevent, INT off);
INT read_io32scaler(char *pevent, INT off);
void clear_io32scaler();
void * ReadoutThread(void *arg);

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {


   {EQ_TRIG_NAME,            /* equipment name */
#ifdef MESADC32_CODE
    {1, 0,                   /* event ID, trigger mask */
#endif
#ifdef V1190_CODE
    {2, 0,                   /* event ID, trigger mask */
#endif
     //     "SYSTEM",              /* event buffer */
     EV_BUFFER,              /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,           /* equipment type */
#else
     EQ_POLLED | EQ_EB,      /* equipment type */
#endif
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,             /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",},
    read_trigger_event,      /* readout routine */
    NULL, NULL,
    NULL,
    }
   ,
   {EQ_SCAL_NAME,            /* equipment name */
#ifdef MESADC32_CODE
    {3, 0,                   /* event ID, trigger mask */
#endif
#ifdef V1190_CODE
    {4, 0,                   /* event ID, trigger mask */
#endif
     "SYSTEM",               /* event buffer */
     EQ_PERIODIC,            /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
     RO_ODB,                 /* and update ODB */
     1000 ,                  /* read every 1 sec */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     1,                      /* log history */
     "", "", "",},
    read_io32scaler,         /* readout routine */
    },
   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

void FillBaseAddress(void) {
  int k = 0, i = 0;

  while (mes32[i].nb) {
    for (int j=0;j<mes32[i].nb;j++) {
      MESADC32_BASE[k] = mes32[i].base[j];
      printf("MESADC32_BASE[0x%x] -> Bank:%s\n", MESADC32_BASE[k], mes32[i].bkname);
      k++;
    }
    i++;
  }
}
	  
/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_Mescallback(INT hDB, INT hseq, void *info)
{
  printf("odb ... Mes trigger settings touched\n");
}

/*-- Sequencer callback info  --------------------------------------*/
void seq_Tdccallback(INT hDB, INT hseq, void *info)
{
  printf("odb ... Tdc trigger settings touched\n");
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  //We do this in any function that accesses the VME bus.
  const std::lock_guard<std::mutex> lock(vme_mutex);

#ifdef MESADC32_CODE
  {
    int size, status;
    char set_str[80];
    cm_set_transition_sequence(TR_START, 401);
    cm_set_transition_sequence(TR_STOP, 401);

    // Extract the base address of all the modules from the MES32 struct definition
    FillBaseAddress();
    
    MESADC32_CONFIG_SETTINGS_STR(mesadc32_config_settings_str);
    
    /* Map MESADC32 settings */
    for (int board=0; board<N_MESADC32; board++) {
      sprintf(set_str, "Initializing board-%01d", board);
      set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");
      printf("Setting/Checking Mesadc32 board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/MADC32-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(mesadc32_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetMes[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(MESADC32_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetMes[board], &tsmes[board], size, MODE_READ
				   , seq_Mescallback, NULL)) != DB_SUCCESS)
	return status;
      
    } // Loop over boards                                                                                      
  }
#endif
  
#ifdef V1190_CODE
  {
    int size, status;
    char set_str[80];
    
    cm_set_transition_sequence(TR_START, 402);
    cm_set_transition_sequence(TR_STOP, 402);

    V1190A_CONFIG_SETTINGS_STR(v1190a_config_settings_str);
    for (int board=0; board<N_V1190A; board++) {
      printf("Setting/Checking V1190A board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/V1190A-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(v1190a_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetTdcA[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(V1190A_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetTdcA[board], &tsTdcA[board], size, MODE_READ
				   , seq_Tdccallback, NULL)) != DB_SUCCESS)
	return status;
    } // Loop over boards                                                                                      
    V1190B_CONFIG_SETTINGS_STR(v1190b_config_settings_str);
    /* Map TDCs settings */
    for (int board=0; board<N_V1190B; board++) {
      printf("Setting/Checking V1190B board %d\n", board);
      sprintf(set_str, "/Equipment/%s/Settings/V1190B-%02d", EQ_TRIG_NAME, board);
      status = db_create_record(hDB, 0, set_str, strcomb(v1190b_config_settings_str));
      status = db_find_key (hDB, 0, set_str, &hSetTdcB[board]);
      if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);
      
      /* Enable hot-link on settings/ of the equipment */
      size = sizeof(V1190B_CONFIG_SETTINGS);
      if ((status = db_open_record(hDB, hSetTdcB[board], &tsTdcB[board], size, MODE_READ
				   , seq_Tdccallback, NULL)) != DB_SUCCESS)
	return status;
    } // Loop over boards
  } // V1190_CODE
#endif
  
  // Open VME interface   
  mvme_open(&myvme, 0);
  gVme = myvme;
  
#ifdef HAVE_IO32_ADC
  if (!io32)
    io32 = new VMENIMIO32(myvme, gVmeio32base);
  
  uint32_t rev = io32->read32(IO32_REVISION); // read firmware revision
  cm_msg(MINFO, frontend_name, "ADC trigger: VMENIMIO32 firmware revision 0x%08x", rev);
  io32->status();
  io32->write32(IO32_NIMOUT, 0x0000); // clear NIM output
  io32->NimOutput(1<<1, 0); // set NIM output 1 (busy)
  //Activate gate and delay generator on output 3 for conversion time blocking.
  io32->SetNimOutputFunction(3,2);
  io32->write32(4*48,0);  //The proper value is determined in begin_run(). 

  io32->write32(IO32_TSC4_ROUTE,0xc0200001); //Enable ext. clock on NIM input 2, div. by 2 and time signals from input 1 in TSC4[0].
  //io32->write32(IO32_NIMIN, 0xFFFF); //XXX:Experimental

  //Route ECL/LVDS input to the scalers (FW upgrade from 0x01120706 to 0x01200721)
  io32->write32(IO32_SCALER_ROUTING,0x7654);
#endif

#ifdef HAVE_IO32_TDC
  if (!io32)
    io32 = new VMENIMIO32(myvme, gVmeio32base);
  
  uint32_t rev = io32->read32(IO32_REVISION); // read firmware revision
  cm_msg(MINFO, frontend_name, "TDC trigger: VMENIMIO32 firmware revision 0x%08x", rev);
  io32->status();

  io32->write32(IO32_NIMOUT, 0x0000); // clear NIM output
  io32->NimOutput(1<<1, 0); // set NIM output 1 (busy)
  io32->SetNimOutputFunction(2, 3); // set NIM output 2 function 3 - V1190 TDC trigger synched with V1190 40 MHz clock
  io32->SetNimOutputFunction(0, 2); //Enable reset on output 0.
  io32->write32(IO32_TSC4_ROUTE,0x00000001); //Time signals from input 1 in TSC4[0].
#endif

#ifdef MESADC32_CODE
  // Setup parameter for EPICS refresh rate
  int size = sizeof(ERRate);
  db_get_value(hDB, 0, "/Misc/Epics Refresh Rate (ms)", &ERRate, &size, TID_DWORD, TRUE);
  db_find_key(hDB, 0, "/Misc/Epics Refresh Rate (ms)", &herrK);
  db_open_record(hDB, herrK, &ERRate, sizeof(ERRate), MODE_READ, NULL, NULL);

  //Loop over modules and do a software reset.
  for(int board=0; board<N_MESADC32; board++) {
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_SOFT_RESET,1);
  }
  usleep(500000); //Modules need at least 200ms to reset. We pause for 500ms.

  //Set ECL termination for last (left) MADC32 board on all three ECL lines, g0, g1 and fc.
  mesadc32_RegisterWrite(myvme, MESADC32_BASE[0], MESADC32_ECL_TERM, 0x7);
  //TODO: For some reason the multicast is giving trouble.
  /*
  //Enable multicast (address: MESADC32_MULTICAST_ADDRESS).
  mesadc32_RegisterWrite(myvme, MESADC32_BASE[0] , MESADC32_CBLT_MCST_CTL, 0xA2); //First module (left)
  for(int board=1; board<N_MESADC32-1; board++) {
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board] , MESADC32_CBLT_MCST_CTL, 0x82); //Mid module.
  }  
  mesadc32_RegisterWrite(myvme, MESADC32_BASE[N_MESADC32-1] , MESADC32_CBLT_MCST_CTL, 0x8A); //Last module (right)
  */

  for(int board=0; board<N_MESADC32; board++) {
    //Set data length format (0=8bit,1=16bit,2=32bit,3=64bit,6=32bit w. fillers)
    //mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_DATA_LEN_FMT,6);   
    //Enable multi-event buffering. Count words not events. Skip Berr.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MULTIEVENT,7); //5 to skip berr
    //Unlimited transfer length (necessary??)
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MAX_TRANSFER_DATA,220);
    //Enable external clock on ECL g1 input.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board],MESADC32_ECL_GATE1_OSC,1); 
    //Bit 0: 0=VME, 1=external, Bit 1: 1=enable external reset.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_TS_SOURCE,3);
    //Time stamp = time / ts_divider.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_TS_DIVIDER, 1);
    //Use also the extended time stamp (0=evt ctr,1=time,3=extended time).
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MARKING_TYPE,1);
    //Use ECL fast clear input to reset time stamp counter.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_ECL_FC_RESET,1);
    //Set almost full level to 8000 data words.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_IRQ_THRESHOLD, 8000); //8000
    //Send signal on NIM output 0 if data in FIFO is above almost full level (8), GG0 (1).
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_NIM_BUSY, 8);
    //Enable internal gate generator gg0.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_USE_GG, 1);
  }
#endif

#ifdef V1190_CODE
  set_equipment_status(EQ_TRIG_NAME, "Initializing TDCs...", "#F0F0AF");

  //-------- TDCs -------------------------------------------
  int csr, temp;

  //frontend_init() setup of the asynchronous DAQ.
  for(int i=0; gTdcBase[i]; i++) v1190_SoftReset(myvme, gTdcBase[i]);
  usleep(500000); //We give it some time to do the reset.
  for(int i=0; gTdcBase[i]; i++){
    char set_str[80];
    printf("--------------- Loading V1190A/B-%01d ---------------\n", i);
    sprintf(set_str, "Loading V1190A/B-%01d", i);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");

    // Check the TDCIN
    csr = mvme_read_value(myvme, gTdcBase[i]+V1190_FIRM_REV_RO);
    printf("Firmware revision: 0x%x\n", csr);
    //We hard-code some settings in frontend_init set the ones from ODB in begin_of_run.
    v1190_MicroWrite(myvme, gTdcBase[i], 0x0500); //Load default config.
    v1190_MicroWrite(myvme, gTdcBase[i], V1190_TRIGGER_MATCH_WO);
    v1190_MicroWrite(myvme, gTdcBase[i], V1190_NO_TRIGGER_TIME_SUBTRACT);
    v1190_MicroWrite(myvme, gTdcBase[i], V1190_DLL_CLOCK_SOURCE);
    v1190_MicroWrite(myvme, gTdcBase[i], 1);  //Use external clock.
    v1190_Write16(myvme, gTdcBase[i],V1190_ALMOST_FULL_LEVEL,0x7500); //~30000 words, max.
    //POUT: 2:almost full, 0: data ready, 1: full
    v1190_Write16(myvme, gTdcBase[i],V1190_OUT_PROG,0x0002);
    uint16_t cr = v1190_Read16(myvme, gTdcBase[i], V1190_CR_RW);
    cr |= (1<<3); // enable empty event
    cr |= (1<<4); // enable align64
    cr |= (1<<8); // enable event fifo
    cr |= (1<<9); // enable extended trigger time tag
    v1190_Write16(myvme, gTdcBase[i], V1190_CR_RW, cr);
  }
#endif

#ifdef MESADC32_CODE
  dataBuffer = new MultiBuffer(N_MESADC32);
  validator = new EventValidator("ADC",N_MESADC32);
  validator->SetTolerance(2); //Allow for two unit deviations
  validator->SetRollOver(30); //30bit wide time stamp without ext. option. 46bit wide with ext. option
#endif
#ifdef V1190_CODE
  dataBuffer = new MultiBuffer(N_V1190A + N_V1190B);
  validator = new EventValidator("TDC",N_V1190A + N_V1190B);
  validator->SetTolerance(2); //Allow for two unit time stamp deviation.
  validator->SetRollOver(27); //27bit wide extended time stamp.
#endif

  set_equipment_status(EQ_TRIG_NAME, "Idle", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Idle", "#F0F0AF");
  printf("frontend_init: done!\n");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  delete dataBuffer;
  delete validator;

  return SUCCESS;
}

volatile std::atomic<bool> gHaveRun(false); //JR: Modified from static int.
volatile std::atomic<int> gEventNo(0);     //Same

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
  //We do this in any function that accesses the VME bus.
  const std::lock_guard<std::mutex> lock(vme_mutex);
  gEventNo = 0;

#ifdef HAVE_IO32_ADC
  int size;

  // program the pulse generator
  int pulser_enabled = FALSE;
  size = sizeof(pulser_enabled);
  db_get_value(hDB, 0, "/Equipment/" EQ_TRIG_NAME "/Settings/IO32/Pulser_enabled", &pulser_enabled, &size, TID_BOOL, TRUE);

  double pulser_freq = 10.0;
  size = sizeof(pulser_freq);
  db_get_value(hDB, 0, "/Equipment/" EQ_TRIG_NAME "/Settings/IO32/Pulser_frequency", &pulser_freq, &size, TID_DOUBLE, TRUE);

  uint32_t pulser_clock = 10;

  uint32_t pulser_period = (1.0e9/pulser_freq)/pulser_clock;
  
  cm_msg(MINFO, frontend_name, "trigger: Pulser period %d: %d ns, %f Hz, enabled %d", pulser_period, pulser_period*pulser_clock, 1.0e9/(pulser_period*pulser_clock), pulser_enabled);

  if (pulser_enabled) {
    io32->SetNimOutputFunction(2, 2); // set NIM output 2 function 2 - pulser
    io32->write32(4*49, pulser_period-1);
    //io32->write32(4*49, 99999); //99999 -> 1kHz
    //io32->write32(4*49, 99);  //99 -> 1MHz
  } else {
    io32->SetNimOutputFunction(2, 0); // set NIM output 2 function 0 - what is it? not a pulser, I hope.
  }
#endif

#ifdef V1190_CODE
  int csr, temp;
  //Set parameters defined by the user through ODB.
  for (int board=0; board<N_V1190A; board++) {
    char set_str[80];
    printf("--------------- Loading V1190A-%01d ---------------\n", board);
    sprintf(set_str, "Loading V1190A-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");

    // Check the TDC
    csr = mvme_read_value(myvme, V1190A_BASE[board]+V1190_FIRM_REV_RO);
    printf("Firmware revision: 0x%x\n", csr);
    printf("--------------- Update V1190A-%01d ---------------\n", board);
    // Update some of the main parameters
    if (tsTdcA[board].leresolution == 100) temp = LE_RESOLUTION_100;  // 0x3
    else if (tsTdcA[board].leresolution == 200)	temp = LE_RESOLUTION_200;  // 0x1
    else if (tsTdcA[board].leresolution == 800)	temp = LE_RESOLUTION_800;  // 0x0
    else temp = 3; // 0x3

    printf("-------------------------------------------------------------resolution: 0x%x \n", temp);
    v1190_LEResolutionSet(myvme, V1190A_BASE[board], temp);
      
    // Adjust window width given in ns
    // 25ns/bin
    temp = (DWORD) abs(tsTdcA[board].windowwidth / 25);
    printf("windowWidth :0x%x\n", (WORD) temp);
    v1190_WidthSet(myvme, V1190A_BASE[board], (WORD) temp);
      
    // Adjust offset width given in ns
    // 10us == 0xE6D, 1us = 0xF5D
    temp = (tsTdcA[board].windowoffset / 25) + 2;
    temp = temp < 0 ? ((DWORD)temp&0xFFF): (DWORD) temp;
    printf("windowOffset :0x%x\n", (WORD) temp);
    v1190_OffsetSet(myvme, V1190A_BASE[board], (WORD) temp);
      
    // Print Current status 
    printf("----------- Status V1190A-%01d ---------------\n", board);
    v1190_Status(myvme, V1190A_BASE[board]);
  }
  for (int board=0; board<N_V1190B; board++) {
    char set_str[80];
    printf("--------------- Loading V1190B-%01d ---------------\n", board);
    sprintf(set_str, "Loading V1190B-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");
    // Check the TDC
    csr = mvme_read_value(myvme, V1190B_BASE[board]+V1190_FIRM_REV_RO);
    printf("Firmware revision: 0x%x\n", csr);
    printf("--------------- Updating V1190B-%01d ---------------\n", board);
    // Update some of the main parameters
    if (tsTdcB[board].leresolution == 100) temp = LE_RESOLUTION_100;
    else if (tsTdcB[board].leresolution == 200)	temp = LE_RESOLUTION_200;
    else if (tsTdcB[board].leresolution == 800)	temp = LE_RESOLUTION_800;
    else temp = 3;
    printf("-------------------------------------------------------------resolution: 0x%x \n", temp);
    v1190_LEResolutionSet(myvme, V1190B_BASE[board], temp);
      
    // Adjust window width given in ns
    // 25ns/bin
    temp = (DWORD) abs(tsTdcB[board].windowwidth / 25);
    printf("windowWidth :0x%x\n", temp);
    v1190_WidthSet(myvme, V1190B_BASE[board], temp);
      
    // Adjust offset width given in ns
    // 10us == 0xE6D, 1us = 0xF5D
    temp = (tsTdcB[board].windowoffset / 25) + 2;
    temp = temp < 0 ? ((DWORD)temp&0xFFF): (DWORD) temp;
    printf("windowOffset :0x%x\n", temp);
    v1190_OffsetSet(myvme, V1190B_BASE[board], temp);

    // Print Current status 
    printf("----------- Status V1190B-%01d ---------------\n", board);
    v1190_Status(myvme, V1190B_BASE[board]);
  }
#endif
    
#ifdef MESADC32_CODE
  //-------- MESADC32 ---------------------------------------
  int status, i;
  uint32_t data;

  //TODO: Multicast instead of loop.
  for (int board=0;board<N_MESADC32;board++) {
    // Stop acquisition (0x603A W=0)
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, MESADC32_ACQ_STOP);
    // Reset Fifo (0x603C W=0)
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_FIFO_RESET, 0);
  }

  //Extract value of Prog. NIM output setting from ODB.
  uint32_t pout_setting = 8;
  size = sizeof(pout_setting);
  db_get_value(hDB, 0, "/Equipment/AdcTrig/Common/Prog. NIM output",&pout_setting, &size, TID_DWORD, TRUE);
  if(pout_setting != 8){
    //This setting is dangerous and only for experts.
    cm_msg(MINFO, frontend_name, "Prog. output setting is 0x%01x. Operation with a setting != 0x8 should only be done by experts.\n",pout_setting);
  }

  //Variable to store estimated maximum busy time for the ADCs.
  double maxTime = -1;

  /* read Mes records settings */
  for (int board=0;board<N_MESADC32;board++) {
    char set_str[80];
    sprintf(set_str, "Loading board-%01d", board);
    set_equipment_status(EQ_TRIG_NAME, set_str, "yellow");

    // Get record from ODB
    size = sizeof(MESADC32_CONFIG_SETTINGS);
    if ((status = db_get_record (hDB, hSetMes[board], &tsmes[board], &size, 0)) != DB_SUCCESS)
      { return status; } 

    // Default setup 
    if (tsmes[board].setup == 0) {
      // Assign the logical address to the module (mod 32) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_MODULE_ID, tsmes[board].modid);
      printf("Mod ID     :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_MODULE_ID));
      
      // Set input range (0x6060 0:4V, 1:10V, 2:8V) 
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_INPUT_RANGE, tsmes[board].input_range);
      printf("Input Range:0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_INPUT_RANGE));

      // Set Delay[0..1] (0x605[0..2] on 8bit 0:25ns, 1:150ns, then x50ns)
      int delay0 = tsmes[board].delay[0];
      int delay1 = tsmes[board].delay[1];
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_0, delay0);
      printf("Delay_0    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_0));
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_1, delay1);
      printf("Delay_1    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_DELAY_1));
      
      // Set Width[0..1] (0x605[4..6] on 8bit x50ns)
      int width0 = tsmes[board].width[0];
      int width1 = tsmes[board].width[1];
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_0, width0);
      printf("Width_0    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_0));
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_1, width1);
      printf("Width_1    :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_HOLD_WIDTH_1));
      
      // Set adc resolution (0x6042 W=0[2K])
      int resolution = tsmes[board].resolution;
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_ADC_RESOLUTION, resolution);
      printf("Resolution :0x%x\n", mesadc32_RegisterRead(myvme, MESADC32_BASE[board], MESADC32_ADC_RESOLUTION));

      //Now, we estimate the time needed for this module to convert one event.
      //This assumes that it is gg0 which is active.
      double t0 = 0; //unit: us
      if(delay0 == 0) t0 += 0.025;
      else if(delay0 == 1) t0 += 0.150;
      else t0 += delay0 * 0.050;
      t0 += width0 * 0.050;
      if(resolution == 0) t0 += 1.8;
      else if(resolution == 1) t0 += 2.7;
      else if(resolution == 2) t0 += 4.3;
      else if(resolution == 3) t0 += 7.6;
      else if(resolution == 4) t0 += 13.7;
      t0 += 2.; //Add 2us for safety.
      printf("Estimated event busy time: %.2lfus\n",t0);
      if(t0 > maxTime) maxTime = t0;

      // Allow reset of all counters (not only ts counter) from ext. reset signal.
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_RESET_CTL_AB, 12);
      //Set the programmable NIM output according to ODB setting.
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_NIM_BUSY, pout_setting);

      // MESADC32 threshold
      for (i=0;i<32;i++) {
	mesadc32_ThresholdSet(myvme, MESADC32_BASE[board], i,  tsmes[board].threshold[i]);
      } 
    }

    mesadc32_Status(myvme, MESADC32_BASE[board]);
  }

  //Then we set the gate/delay blocking according to the slowest module.
  printf("Maximum estimated ADC event busy time: %.2lfus\n",maxTime);
  uint32_t blockingWidth = 0x00000000;
  blockingWidth += maxTime / 0.01; //Convert to units of 10ns.
  uint32_t widthSetting = 0x00000000;
  widthSetting |= blockingWidth << 16;
  widthSetting += 1;
  //printf("blockingWidth = 0x%08x\n",widthSetting);
  io32->write32(4*48,widthSetting);

#endif //MESADC32_CODE

#ifdef MESADC32_CODE
  //Enable (arm) ext. reset on NIM input 0.
  io32->write32(IO32_SCALERS_TS,0x0001);

  for (int board=0;board<N_MESADC32;board++) {
    //Enable single shot external reset.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_RESET_CTL_AB, 12);
    //Start acquisition.
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, 1);
  }
#endif
#ifdef V1190_CODE
  //Start acquisition.
  for (int i=0; gTdcBase[i]; i++) {
    v1190_SoftClear(gVme, gTdcBase[i]);
  }
#endif
  //Ok, we finished the run-specific configuration of the modules. Now we start
  //the readout-thread.
  stop = false;
  abort_run = false;
  if(pthread_create(&readout_thread, NULL, ReadoutThread, NULL)) {
    printf("begin_of_run(): Error creating readout thread\n");
  }

#ifdef V1190_CODE

  //Emit reset. Should reset both IO32 modules (and possibly other modules as well.).
  //Ideally we would want some kind of signal telling that the ADC IO32 is ready.
  printf("begin_of_run(): Synchronising clocks...\n");
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_TS);
#endif
  printf("begin_of_run(): Resetting counters...\n");
  //Reset scaler.
  clear_io32scaler();  //Issues IO32_CMD_RESET_SCALERS
  io32->write32(IO32_TRIG_COUNT, 0); // reset trigger counter (?).
  io32->write32(IO32_TRIG_TS,0); //Reset trigger time stamp.
  io32->write32(IO32_NIMIN, 0xFFFF); // XXX: Experiment
  //Clear TSC4 FIFO.
  //printf("begin_of_run(): Resetting TSC4 FIFO\n");
  io32->write32(IO32_TSC4_FIFO,0x0000); //reset TSC4 FIFO (?).

  //Clearing buffers.
  dataBuffer->clear();
  validator->Clear();
  tscBuffer.clear();

  usleep(1e5); //We give the module 100ms to reset (maybe not necessary).
  //Clear busy
  io32->NimOutput(0, 1<<1);

  // Cosmetics on Web
  set_equipment_status(EQ_TRIG_NAME, "Running", "#00FF00");
  //set_equipment_status(EQ_SCAL_NAME, "Running", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Running", "#00FF00");

  gHaveRun = true;

  cm_msg(MINFO, frontend_name, "begin run %d", run_number);
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  static bool gInsideEndRun = false;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  gHaveRun = false;
  int local_gEventNo = gEventNo;
  cm_msg(MINFO, frontend_name, "end of run %d, %d events, %d errors",run_number, local_gEventNo, 0);

  gInsideEndRun = false;

#if 0
  for (int board=0;board<N_MESADC32;board++) {
    if (tsmes[board].mscf_presence) {
      for (int mscf=0;mscf<tsmes[board].mscf_presence;mscf++) {
      }
    }
  }
#endif


#ifdef V1190_CODE
  printf("end_of_run(): TDC\n");
#endif

  printf("end_of_run(): Waiting to acquire VME.\n");
  //We do this in any function that accesses the VME bus.
  //const std::lock_guard<std::mutex> lock(vme_mutex);
  vme_mutex.lock();
  printf("end_of_run(): VME locked.\n");

  //Set busy
  io32->NimOutput(1<<1,0);
#ifdef MESADC32_CODE
  printf("end_of_run(): ADC\n");
  //Stop acquisition. TODO: Multicast.
  for (int board=0;board<N_MESADC32;board++) {
    mesadc32_RegisterWrite(myvme, MESADC32_BASE[board], MESADC32_START_ACQ, 0 );
  }
#endif
  vme_mutex.unlock();
  printf("end_of_run(): VME released.\n");
  //Signal and wait for the readout- and validate-threads to return.
  printf("end_of_run(): Stopping readout thread.\n");
  stop = true;
  abort_run = false;
  pthread_join(readout_thread, NULL);
  //pthread_join(validate_thread, NULL);

  printf("end_of_run(): Readout thread stopped.\n");

  set_equipment_status(EQ_TRIG_NAME, "Idle", "#F0F0AF");
  set_equipment_status(EQ_SCAL_NAME, "Idle", "#F0F0AF");

  //char str[32];
  //sprintf(str, "Taken %d", gATscaler);
  //set_equipment_status(EQ_SCAL_NAME, str, "#F0F0AF");
  cm_msg(MINFO, frontend_name, "End of run completed.\n");
  return SUCCESS;
}

#ifdef MESADC32_CODE

void * ReadoutThread(void *arg)
{
  //We keep track of readout times.
  std::array<time_t,16> last_readout;
  last_readout.fill(time(NULL));
  bool blocked = false;
  
  uint32_t data[256];
  while(1){
    const std::lock_guard<std::mutex> lock(vme_mutex);
    //First empty TSC4 FIFO.
    uint32_t ctrl_word = io32->read32(4*12); // TSC control register
    if(ctrl_word & 0x8000){
      //Handle it.
      cm_msg(MERROR, frontend_name, "Overflow in VMEIO32 TSC4 buffer! Read control word 0x%08x from register 12.\n", ctrl_word);
      abort_run = true;
      return NULL; //Maybe not necessary...
    }
    int wc = ctrl_word & 0x7FFF;
    for (int i=0; i<wc; i++){
      uint32_t word = io32->read32(IO32_TSC4_FIFO); // read TSC FIFO
      tscBuffer.push(word);
    }
    uint32_t nWordsMax = 0;
    unsigned int moduleID = 0;

    //For each 'turn' we find the module with the most data.
    for(unsigned int i=0; i<N_MESADC32; i++){
      if(!(dataBuffer->empty(i) || validator->Complete(i))){
        //Data is ready to be transferred!
        do{
          //printf("line 1290\n");
          //As long as the data buffer does not run empty, try to move data from
          //the buffer to the validator. When the validator has received a
          //complete event TryPush will fail and we break the loop.
          //if(first.at(i))
          //printf("Buffer %d trying to push 0x%08x\n",i,dataBuffer->front(i));
          int status = validator->TryPush(i,dataBuffer->front(i));
          //printf("  status = %d.\n",status);
          if(status != EVT_SUCCESS){
            //if((dataBuffer->front(i) & 0xc0000000) != 0x40000000){
              //Failed, but not a BOE word!
            //  std::shared_ptr<ModuleEvent> evt_i = validator->GetEvents().at(i);
            //  printf("Validator:\n");
            //  while(!evt_i->Empty()){
            //    printf("  0x%08x\n",evt_i->Front());
            //    evt_i->Pop();
            //  }
            //  printf("Buffer:\n");
            //  while(!dataBuffer->empty(i)){
            //    printf("  0x%08x\n",dataBuffer->front(i));
            //    dataBuffer->pop(i);
            //  }
              //exit(EXIT_FAILURE);
            //}
            if(status == EVT_FULL) break;
            else{
              cm_msg(MERROR, frontend_name, "Failed trying to validate data word 0x%08x. Error status %d (see ModuleEvent.h).\n",dataBuffer->front(i),status);
              abort_run = true;
              return NULL;
            }
          }
          //if(gEventNo > 1000){
          //  printf("ReadoutThread(): Requesting hard stop.\n");
          //  hard_stop();
          //}
          //if(first.at(i))
          //printf("  ..success.\n");
          //If the word was successfully moved from the buffer, pop it.
          dataBuffer->pop(i);
        }
        while(!dataBuffer->empty(i));
        //first.at(i) = false;
      }
      //printf("line 1322\n");
      //Ask how many words are in the module FIFO.
      //XXX: This could maybe be done in a single DMA transaction.
      uint32_t address = MESADC32_BASE[i];
      //uint32_t dataReady = mesadc32_RegisterRead(myvme, address, MESADC32_DATA_READY);
      //if(!dataReady) continue;
      uint32_t nWords = mesadc32_RegisterRead(myvme, address, MESADC32_BUF_DATA_LEN);
      if(nWords > nWordsMax){
        nWordsMax = nWords;
        moduleID = i;
      }
      //printf("line 1333\n");
      //if(nWords > 0) printf("Module %d has %d 64-bit words.\n",i,nWords);
    }
    //printf("line 1336\n");
    time_t now = time(NULL);
     //More than 100 64-bit words? Longer than 1s since last readout?
    //printf("Module %d has %d 64-bit words.\n",moduleID,nWordsMax);
    //printf("Size of last_readout: %d,\n",last_readout.size());
    if(nWordsMax > 100 || now - last_readout.at(moduleID) > 1){
      //printf("line 1340\n");
      //static int N = 0;
      //N++;
      //if(N>47) exit(EXIT_FAILURE);
      //We can maximally read 256 32-bit words
      uint32_t readWords = 256;//2*nWordsMax < 16 ? 2*nWordsMax : 16;
      //printf("Module %d reading %d 32-bit words.\n",moduleID,readWords);
      //32-bit block transfer.
      mvme_set_am(myvme, MVME_AM_A32_SB);
      mvme_set_dmode(myvme,MVME_DMODE_D32);      
      mvme_set_blt(myvme,MVME_BLT_BLT32);
      int flag = mvme_read(myvme, data, MESADC32_BASE[moduleID], sizeof(DWORD)*readWords);
      mesadc32_RegisterWrite(myvme, MESADC32_BASE[moduleID], MESADC32_READOUT_RESET, 1);
      last_readout.at(moduleID) = time(NULL);
      //printf("line 1354\n");
/*
      //64-bit block transfer.
      mvme_set_am(myvme, MVME_AM_A32_SMBLT);
      mvme_set_dmode(myvme,MVME_DMODE_D32);      
      mvme_set_blt(myvme,MVME_BLT_MBLT64);
      int flag = mvme_read(myvme, data, MESADC32_BASE[moduleID], sizeof(DWORD)*readWords);
      //mesadc32_RegisterWrite(myvme, MESADC32_BASE[moduleID], MESADC32_READOUT_RESET,0);
*/
/*
      //32-bit single word transfer
      for(int j=0; j<readWords; j++){
        mvme_set_am(myvme, MVME_AM_A24);
        mvme_set_dmode(myvme, MVME_DMODE_D32);
        data[j] = mvme_read_value(myvme, MESADC32_BASE[moduleID]+MESADC32_EVENT_READOUT_BUFFER);
        //mesadc32_RegisterWrite(myvme, MESADC32_BASE[moduleID], MESADC32_READOUT_RESET, 0);
      }
*/
      //int flag = MVME_SUCCESS;
      if(flag == MVME_SUCCESS){
        //printf("line 1374\n");
        //unsigned int j;
        //Push the data directly into the data buffer.
        //uint32_t high_ts;
        //uint32_t low_ts;
        //if(1){//if(readWords % 2 != 0){
        //  uint32_t last_word = 0;
        //  printf("Read from module %d:\n",moduleID);
        //  for(j=0; j<readWords; j++){
        //    printf("  0x%08x\n",data[j]);
        //    uint32_t word = data[j];
        //    if(((word & 0xc0000000) == 0xc0000000 && (last_word & 0xc0000000) == 0xc0000000)
        //      || ((word & 0xc0000000) == 0x40000000 && (last_word & 0x40000000) == 0xc0000000)){
        //      printf("!-------------------------------------------!\n");
        //    }
        //    last_word = word;
        //  }
          //printf("earlier read:\n");
          //for(j=0; j<last_read.at(moduleID).size(); j++){
          //  printf("  0x%08x\n",last_read.at(moduleID).at(j));
          //}
          //static int errorCount = 0;
          //if(errorCount > 100) exit(EXIT_FAILURE);
          //errorCount++;
        //}
        for(unsigned int j=0; j<readWords; j++){
          //printf("  0x%08x\n",data[j]);
          //if((data[j] & 0xff800000) == 0x04800000){
          //  if((previous.at(moduleID) & 0xc0000000) != 0x40000000){
          //    printf("Word 0x%08x preceded by 0x%08x\n",data[j],previous.at(moduleID));
          //  }
          //}
          //uint32_t word = data[j];
          //uint32_t last_word = previous.at(moduleID);
          //numRead.at(moduleID)++;
          //if(((word & 0xc0000000) == 0xc0000000 && (last_word & 0xc0000000) == 0xc0000000)
          //|| ((word & 0xc0000000) == 0x40000000 && (last_word & 0xc0000000) == 0x40000000)){
          //  printf("  0x%08x followed by 0x%08x\n",last_word,word);
          //  printf("Word no. %d from module %d\n",numRead.at(moduleID),moduleID);
          //}
          
          //if((data[j] & 0xff800000) == 0x04800000) high_ts = data[j] & 0x0000ffff;
          //if((data[j] & 0xc0000000) == 0xc0000000) low_ts = data[j] & 0x3fffffff;
          //if((data[j] & 0xc0000000) == 0xc0000000) printf("Received 0x%08x from ADC module %2d at 0x%08x\n",data[j],moduleID,MESADC32_BASE[moduleID]);
          //if((data[j] & 0xc0000000) == 0x80000000)
          //  printf("Received 0x%08x from ADC module %2d\n",data[j],moduleID);
          //  uint32_t low_ts = low_ts = data[j] & 0x3fffffff;
          //  printf("TS = %lfms from ADC module %2d at 0x%08x\n",low_ts*0.0001,moduleID,MESADC32_BASE[moduleID]);
          //}
          if(data[j] == 0x80000000 || data[j] == 0xffffffff) break; //Reached end of transfer.
          dataBuffer->push(moduleID,data[j]);
          //if(first.at(moduleID))
            //printf("Received 0x%08x from ADC module %2d\n",data[j],moduleID);
          //if(data[j] != 0xffffffff && data[j] != 0x80000000)
          //previous.at(moduleID) = data[j];
          //last_read.at(moduleID).push_back(data[j]);
        }
        //printf("High stamp = 0x%08x, low stamp = 0x%08x for module %d\n",high_ts, low_ts, moduleID);
      }
      else{
        //printf("ReadoutThread(): Problem reading data.\n");
        //exit(EXIT_FAILURE);
        cm_msg(MERROR, frontend_name,"Problem reading data from ADC %d at base address 0x%08x! Status flag %d.\n",moduleID,MESADC32_BASE[moduleID],flag);
        abort_run = true;
        //cm_transition(TR_STOP,0,NULL,0,1,2); //TR_SYNC: 0, TR_ASYNC: 1
        return NULL;
      } 
    }
    //else if(stop && nWordsMax==0 && dataBuffer->empty()){
    if(stop){ //XXX: Only for testing!
      printf("ReadoutThread(): Received stop signal.\n");
      //readout_active = false;
      //cm_transition(TR_STOP,0,NULL,0,TR_SYNC,2); //TR_SYNC: 0, TR_ASYNC: 1
      //printf("ReadoutThread(): Done waiting for cm_transition().\n");
      break; //Ok, no data left in modules, so we break the loop.
    }
    //We attempt to keep the size of the data buffer manageable.
    if(dataBuffer->size() > maxBufferSize && !blocked){
      //Block the trigger
      io32->NimOutput(1<<1,0);
      blocked = true;
      printf("SW buffer full. Blocking trigger.\n");
    }
    else if(blocked && dataBuffer->size() < minBufferSize && gHaveRun){
      //Unblock trigger.
      io32->NimOutput(0, 1<<1);
      blocked = false;
      printf("SW buffer only half full. Unblocking trigger.\n");
    }
  }
  printf("ReadoutThread(): Exiting ADC readout thread.\n");
  return NULL;
}
#endif
#ifdef V1190_CODE
void * ReadoutThread(void *arg)
{
  bool blocked = false;
  uint32_t data[256];
  int bufferContent[N_V1190A + N_V1190B] = {0};
  while(1){
    //pthread_testcancel();
    const std::lock_guard<std::mutex> lock(vme_mutex);
    //First empty TSC4 FIFO.
    uint32_t ctrl_word = io32->read32(4*12); // TSC control register
    if(ctrl_word & 0x8000){
      //printf("ReadoutThread(): TSC4 overflow\n");
      //Handle it.
      //exit(EXIT_FAILURE);
      cm_msg(MERROR, frontend_name, "Overflow in VMEIO32 TSC4 buffer! Read control word 0x%08x from register 12.\n",ctrl_word);
      abort_run = true;
      //cm_transition(TR_STOP,0,NULL,0,1,2); //SYNC: 0, ASYNC: 1
      return NULL;
    }
    int wc = ctrl_word & 0x7FFF;
    for (int i=0; i<wc; i++){
      uint32_t word = io32->read32(IO32_TSC4_FIFO); // read TSC FIFO
      tscBuffer.push(word);
    }
    //usleep(1e6);
    
    for(int t=0; gTdcBase[t] != 0; t++){
      uint16_t value = v1190_Read16(myvme,gTdcBase[t],0x1002);
      //uint16_t data_ready_mask = 1<<0;
      //uint16_t almost_full_mask = 1<<1;
      //uint16_t full_mask = 1<<2;
      //uint16_t trg_match_mask = 1<<3;
      uint16_t trg_lost_mask = 1<<15;
      //if((value & data_ready_mask) == data_ready_mask) printf("TDC module %d has data.\n",t);
      //if((value & almost_full_mask) == almost_full_mask) printf("TDC module %d almost full.\n",t);
      //if((value & full_mask) == full_mask) printf("TDC module %d is full.\n",t);
      //if((value & trg_match_mask) == trg_match_mask) printf("TDC module %d is in trigger matching mode.\n",t);
      if((value & trg_lost_mask) == trg_lost_mask){
        //printf("TDC module %d lost a trigger.\n",t);
        //exit(EXIT_FAILURE);
        cm_msg(MERROR, frontend_name,"Trigger lost in TDC %d at base address 0x%08x! Value read from control register 0x1002: 0x%04x.\n",t,gTdcBase[t],value);
        //cm_transition(TR_STOP,0,NULL,0,1,2); //SYNC: 0, ASYNC: 1
        abort_run = true;
        return NULL;
      }
    }
    
    //if(stop) break;
    uint32_t nWordsMax = 0;
    unsigned int moduleID = 0;
    //unsigned int i;
    //static int k=-1;
    //k++;
    //static bool armed = true;
    for (int t=0; gTdcBase[t] != 0; t++){
      //The following was moved here from the ValidateThread.
      if(!(dataBuffer->empty(t) || validator->Complete(t))){
        //Data is ready to be transferred!
        do{
          //As long as the data buffer does not run empty, try to move data from
          //the buffer to the validator. When the validator has received a
          //complete event TryPush will fail and we break the loop.
          int status = validator->TryPush(t,dataBuffer->front(t));
          if(status != EVT_SUCCESS){
            if(status == EVT_FULL) break;
            else{
              cm_msg(MERROR, frontend_name, "Failed trying to validate data word 0x%08x.\n",dataBuffer->front(t));
              abort_run = true;
              return NULL;
            }
          }
          //If the word was successfully moved from the buffer, pop it.
          dataBuffer->pop(t);
        }
        while(!dataBuffer->empty(t));
      }
      uint32_t address = gTdcBase[t];
      uint16_t word = v1190_Read16(myvme,address,0x103c);
      uint16_t nEvents = word & 0x07ff; //Words in event FIFO.
      //if(!nEvents) continue;
      //if(nEvents>0) printf("Module %d has %d words in event FIFO.\n",t,nEvents);
      for(unsigned int i=0; i<nEvents; i++){
        uint32_t fifoWord = v1190_Read32(myvme,address,0x1038); //Read event FIFO.
        //printf("Received 0x%08x from event FIFO %d\n",fifoWord,t);
        uint32_t eventSize = fifoWord & 0x0000ffff;
        bufferContent[t] += eventSize;
      }
      if(bufferContent[t] > nWordsMax){
        nWordsMax = bufferContent[t];
        moduleID = t;
      }
      //if(bufferContent[t]>0){
      //  if(k%10000 == 0){
      //    printf("Module %d has %d words.\n",t,bufferContent[t]);
      //  }
      //}
      //word = v1190_Read16(myvme,address,0x1002);
      //uint16_t almost_full_mask = 1<<1;
      //if((word & almost_full_mask) == almost_full_mask && armed){
      //  printf("  TDC module %d almost full.\n",t);
      //  armed = false;
      //}
    }
    //if(nWordsMax > 30000) printf("nWordsMax = %d\n",nWordsMax);
    if(nWordsMax > 0){
      //We can maximally read 256 32-bit words
      uint32_t readWords = nWordsMax < 256 ? nWordsMax : 256;
      int wordCount = v1190_DataRead(myvme, gTdcBase[moduleID], data, readWords);
      if(wordCount == readWords){
        bufferContent[moduleID] -= readWords;
        //unsigned int j;
        //uint32_t ext_ts;
        //Push the data directly into the data buffer.
        for(unsigned int j=0; j<readWords; j++){
          //if((data[j] & 0x88000000) == 0x88000000) ext_ts = data[j] & 0x07ffffff;
          //if((data[j] & 0x88000000) == 0x88000000) printf("Received 0x%08x from TDC module %d\n",data[j],moduleID);
          //if((data[j] & 0x88000000) == 0x88000000){
          //  uint32_t ext_ts = data[j] & 0x07ffffff;
          //STOP  printf("TS = %lfms from TDC module %d\n",ext_ts*0.0008,moduleID);
          //}
          dataBuffer->push(moduleID,data[j]);
        }
        //printf("Ext stamp = 0x%08x from module %d\n",ext_ts,moduleID);
      }
      else{
        //printf("ReadoutThread(): Problem reading data.\n");
        //exit(EXIT_FAILURE);
        cm_msg(MERROR, frontend_name,"Problem reading data from TDC %d at base address 0x%08x! Words requested: %d, words received: %d.\n",moduleID,gTdcBase[moduleID],readWords,wordCount);
        abort_run = true;
        //cm_transition(TR_STOP,0,NULL,0,1,2); //SYNC: 0, ASYNC: 1
        return NULL;
      }
    }
    //else if(stop && dataBuffer->empty()){
    if(stop){  //XXX: Only for testing!
      printf("ReadoutThread(): Received stop signal.\n");
      //readout_active = false;
      break; //Ok, no data left in modules, so we break the loop.
    }
    //We attempt to keep the size of the data buffer manageable.
    if(dataBuffer->size() > maxBufferSize && !blocked){
      //Block the trigger
      io32->NimOutput(1<<1,0);
      blocked = true;
      printf("SW buffer full. Blocking trigger.\n");
    }
    else if(blocked && dataBuffer->size() < minBufferSize && gHaveRun){
      //Unblock trigger.
      io32->NimOutput(0, 1<<1);
      blocked = false;
      printf("SW buffer only half full. Unblocking trigger.\n");
    }
  }
  printf("ReadoutThread(): Exiting TDC readout thread.\n");
  return NULL;
}
#endif

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  const std::lock_guard<std::mutex> lock(vme_mutex);
  set_equipment_status(EQ_TRIG_NAME, "Paused", "#A0A0FF");
  gHaveRun = false;
  io32->NimOutput(1<<1, 0); // Set busy output level
  
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  const std::lock_guard<std::mutex> lock(vme_mutex);
  set_equipment_status(EQ_TRIG_NAME, "Running", "#00FF00");
  gHaveRun = true;
  io32->NimOutput(0, 1<<1); // Clear busy output level
  
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
#ifdef MESADC32_CODE
// EPICS
//-PAA added #19 SuOR, #23 Sd1OR
#define N_EPICS 6  // No more than 19/20 (last is the watchdog)
static HNDLE hEpicsVar = 0;
static uint32_t lasttime=0;
static float epicsVar[N_EPICS];
#endif

INT frontend_loop() {

#ifdef MESADC32_CODE
  // EPICS
  if (hEpicsVar == 0) {
    // First time in get key
    int status = db_find_key (hDB, 0, "Equipment/Beamline/Variables/Demand", &hEpicsVar);
    if (status != DB_SUCCESS) {
      cm_msg(MINFO,"FE","EPICS demand not found");
      hEpicsVar = 0;
    } 
  } else {
    if (ss_millitime() - lasttime > ERRate) { // Every 100ms using _index as the 19 is controlled by feEpics
      // Periodically copy local scaler to Epics equipment (BeamLine)
      db_set_data_index(hDB, hEpicsVar, &epicsVar[6], sizeof(float),  0, TID_FLOAT); // SID2
      db_set_data_index(hDB, hEpicsVar, &epicsVar[4], sizeof(float),  1, TID_FLOAT); // SuOR
      db_set_data_index(hDB, hEpicsVar, &epicsVar[5], sizeof(float),  2, TID_FLOAT); // Sd1OR
      db_set_data_index(hDB, hEpicsVar, &epicsVar[3], sizeof(float),  8, TID_FLOAT); // SSB
      db_set_data_index(hDB, hEpicsVar, &epicsVar[0], sizeof(float), 10, TID_FLOAT); // Scint
      db_set_data_index(hDB, hEpicsVar, &epicsVar[1], sizeof(float), 11, TID_FLOAT); // Ion chamber
      db_set_data_index(hDB, hEpicsVar, &epicsVar[2], sizeof(float), 12, TID_FLOAT); // FreeTrg
      //      printf("loop: update: %f %f\n", epicsVar[0], epicsVar[1]);
      lasttime = ss_time();
    }
  }  
#endif

  //If anyone asked to abort, we request a transition.
  if(abort_run && gHaveRun){
    //First, raise alarm.
    char alarm[32] = {0};
    int size = sizeof(alarm);
    db_get_value(hDB, 0, "Alarms/Alarms/Run aborted/Alarm Class", alarm, &size, TID_STRING, TRUE);
    if (alarm[0]){
      al_trigger_alarm("Run aborted", "Run aborted due to an error. See message log.", alarm, "", AT_INTERNAL);
    }
    //Then request run transition.
    cm_msg(MERROR, frontend_name,"Aborting run...\n");
    cm_transition(TR_STOP,0,NULL,0,TR_ASYNC,2); //SYNC: 0, ASYNC: 1
  }

  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

char bars[] = "|\\-/";
int i_bar;

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
     /* Polling routine for events. Returns TRUE if event
	is available. If test equals TRUE, don't return. The test
	flag is used to time the polling */
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE))
    //    if (lam > 10)
{
  //We check if a complete event is ready.
  for (int i = 0; i < count; i++) {
    int lam = 0;
    if(validator->Complete() && tscBuffer.size()) lam = 1;
    if (lam) if (!test) return lam;
  }
  
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

//
//---------------------------------------------------------------
int clear_scalers()
{
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_SCALERS);
  return 0;
}

/*-- Event readout -------------------------------------------------*/
INT read_trigger_event(char *pevent, INT off)
{
  //printf("In read_trigger_event\n");
  // > THE LINE BELOW SHOULD BE HERE AND NOWHERE ELSE < !
  bk_init32(pevent);

  //Here we include the code for asynchronous readout.
  uint32_t *pdata;
  //We validate the event. If incomplete, emit error and request abort.
  if(!validator->CheckConsistency()){
    if(!abort_run){
      cm_msg(MERROR, frontend_name,"Event inconsistent.\n"); //Only print once.
      printf("poll_event(): Event inconsistent.\n");
    }
    abort_run = true;
    return 0;
  }

  std::vector<std::shared_ptr<ModuleEvent>> &event = validator->GetEvents();
#ifdef MESADC32_CODE
  int m = 0;
  //Loop over all the groups
  for(int i=0; i<nAdcBanks; i++){
    // create MESADC32 bank for the group
    bk_create(pevent, mes32[i].bkname, TID_DWORD, (void **) &pdata);
    // Add all the modules to the current bank
    for (int j=0 ; j<mes32[i].nb ; j++) {
      std::shared_ptr<ModuleEvent> evt_m = event.at(m);
      //while(!evt_m->Empty()){
      unsigned int size = evt_m->Size(); //XXX: The while loop was not thread safe!!
      for(int k=0; k<size; k++){
        *pdata = evt_m->Front();
        //printf("Evt. %d: 0x%08x\n",m,pdata[0]);
        evt_m->Pop();
        pdata++;
      }
      m++;
    }
    // Close bank for the current group
    bk_close(pevent, pdata);
  }
#endif
#ifdef V1190_CODE
  for(int i=0; gTdcBase[i]!=0; i++){
    bk_create(pevent,gTdcBkName[i],TID_DWORD,(void**)&pdata);
    std::shared_ptr<ModuleEvent> evt_i = event.at(i);
    //while(!evt_i->Empty()){
    unsigned int size = evt_i->Size();
    for(int k=0; k<size; k++){
      *pdata = evt_i->Front();
      //printf("0x%08x\n",pdata[0]);
      evt_i->Pop();
      pdata++;
    }
    bk_close(pevent,pdata);
  }
#endif //V1190_CODE
  //}
  bool have_data = true;
  // Compose Readout Bank statistics
  {
    //We grab the trigger time stamp.
    uint32_t trig_time = 0;
    if(gEventNo == 0){
      //We have to treat the first event specially, because the TSC4 FIFO
      //appears to have garbage before we get the actual first time stamp.
      const std::lock_guard<std::mutex> lock(vme_mutex);
      uint32_t ts0  = io32->read32(IO32_TRIG_TS); // trigger timestamp
      uint32_t trig_count = io32->read32(IO32_TRIG_COUNT); // trigger counter
      while(tscBuffer.size()){
        uint32_t ts = tscBuffer.front();
        tscBuffer.pop();
        //I observed a tiny discrepancy sometime, so we allow for two units diff.
        uint32_t diff = ts > ts0 ? ts - ts0 : ts0 - ts;
        if(diff <= 2){ //discrepancy <= 0.1us.
          trig_time = ts;
          break;
        }
      }
      if(trig_time == 0 || trig_count != 1){
        //printf("read_trigger_event(): TSC4 problem.\n");
        //Handle it.
        //exit(EXIT_FAILURE);
        if(!abort_run) cm_msg(MERROR, frontend_name,"Problem emptying VMEIO32 TSC4 buffer for event no. 0.\n");
        //cm_transition(TR_STOP,0,NULL,0,1,2); //SYNC: 0, ASYNC: 1
        abort_run = true;
        return 0;
      }
    }
    else{
      trig_time = tscBuffer.front();
      tscBuffer.pop();
    }
    //printf("read_trigger_event(): Evt. no. %d, TSC4 TS 0x%08x\n",gEventNo,trig_time);

    uint32_t *pdata32;
#if defined V1190_CODE
    bk_create(pevent, "VTRT", TID_DWORD, (void **) &pdata32);
    *pdata32++ = 0xbbbb0010;  // 0 - IRIS TDC header and version
#endif
#if defined MESADC32_CODE
    bk_create(pevent, "VTRA", TID_DWORD, (void **) &pdata32);
    *pdata32++ = 0xaaaa0010;  // 0 - IRIS ADC header and version
#endif 
    *pdata32++ = gEventNo;  // 1 - event number (from sw), counting from 0.
    *pdata32++ = trig_time; // 2 - trigger timestamp from TSC4 FIFO.
    *pdata32++ = 0;  // 3 - readout start time
    *pdata32++ = 0;  // 4 - readout end time
    *pdata32++ = 0;  // 5 - trigger latency (trigger time - start of readout time)
    *pdata32++ = 0;  // 6 - readout elapsed time
    *pdata32++ = 0;  // 7 - busy elapsed time
    bk_close(pevent, pdata32);
    
  }
 
  if (have_data) {
    gEventNo ++;
    return bk_size(pevent);
  } else {
    return 0;
  }
}

const int kMaxIo32scalers = 32;
uint32_t io32scalerSum[kMaxIo32scalers];

//--------------------------------------------------------------------
void clear_io32scaler()
{
  io32->write32(IO32_COMMAND, IO32_CMD_RESET_SCALERS);
  memset(io32scalerSum, 0, sizeof(io32scalerSum));
}

//--------------------------------------------------------------------
INT read_io32scaler(char *pevent, INT off)
{
  const std::lock_guard<std::mutex> lock(vme_mutex);

  if (0) {
    char str[256];
    gATscaler = SERIAL_NUMBER(pevent);
    if (run_state == RO_RUNNING) {
      sprintf(str, "Running %d", gATscaler);
      set_equipment_status(EQ_SCAL_NAME, str, "#A0A0FF");
    } else {
      sprintf(str, "Taken %d", gATscaler);
      set_equipment_status(EQ_SCAL_NAME, str, "#F0F0AF");
    }
  }

  int numsc  = 32;
  int iclock = 15;
  //int iclock = 31; //Changed in FW 0x01121128

  io32->write32(IO32_COMMAND, IO32_CMD_LATCH_SCALERS);

  /* init bank structure */
  bk_init32(pevent);

  uint32_t psc[numsc];

  // Scaler Data
  {
    uint32_t *pdata32;

#ifdef V1190_CODE
    bk_create(pevent, "SCTD", TID_DWORD, (void **) &pdata32);
#else
    bk_create(pevent, "SCAD", TID_DWORD, (void **) &pdata32);
#endif

    //psc = pdata32;

    int count = io32->ReadScaler(pdata32, numsc);
    assert(count == numsc);

    //Rerouting of scalers after FW 0x01121128
    memcpy(psc, pdata32+16, 16*sizeof(uint32_t));
    memcpy(psc+16, pdata32, 16*sizeof(uint32_t));
    memcpy(pdata32, psc, 32*sizeof(uint32_t));

    pdata32 += numsc;
    
    bk_close(pevent, pdata32);
  }

#ifdef V1190_CODE  
  double dt = 50.0*psc[iclock]/1e9; // 50 ns clock (20MHz)
#else
  double dt = 200.0*psc[iclock]/1e9; // 200 ns clock (5MHz), remember to change
#endif
  double dt1 = 1.0/dt;
  //printf("dt = %f\n", dt);

  // Scaler Rate
  {
    double* pdata;
#ifdef V1190_CODE
    bk_create(pevent, "SCTR", TID_DOUBLE, (void **) &pdata);
#else
    bk_create(pevent, "SCAR", TID_DOUBLE, (void **) &pdata);
#endif
    //printf("Scaler rates at 1/dt = %lf:\n",dt1);
    for (int i=0; i<numsc; i++) {
      //printf("  %d: %lf\n",i,psc[i]*dt1);
      *pdata++ = dt1*psc[i];
#if defined MESADC32_CODE
      //Read ADC scaler (PPG32), copy to epics temp array)
      if (i == 29) epicsVar[0] = dt1*psc[i]; // Rate Scintillator
      if (i == 30) epicsVar[1] = dt1*psc[i]; // Rate Ion Chamber
      if (i == 31) epicsVar[2] = dt1*psc[i]; // Rate Free Trigger
      if (i == 27) epicsVar[3] = dt1*psc[i]; // Rate SSB
      if (i == 19) epicsVar[4] = dt1*psc[i]; // SuOR
      if (i == 23) epicsVar[5] = dt1*psc[i]; // Sd1OR
      if (i == 16) epicsVar[6] = dt1*psc[i]; // SID2
#endif
    }
    bk_close(pevent, pdata);
  }

  // Scaler SUM
  {
    DWORD* pdata;
#ifdef V1190_CODE
    bk_create(pevent, "SCTS", TID_DWORD, (void **) &pdata);
#else
    bk_create(pevent, "SCAS", TID_DWORD, (void **) &pdata);
#endif
    //printf("Scaler sums:\n");
    for (int i=0; i<numsc; i++) {
      io32scalerSum[i] += psc[i];
      *pdata++ = io32scalerSum[i];
      //printf("  %d: %d\n",i,io32scalerSum[i]);
    }
    bk_close(pevent, pdata);
  }

  return bk_size(pevent);
}

// end file
